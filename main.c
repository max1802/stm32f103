#include "M3_Core.h"
#include "math.h"

float u;
float u1;
int k;

int DATA1[8] = {0};
int count = 0;
int count_DR = 0; //
u8 Flag_Pack = 0; //0 when slave can get a frame, 1 when slave can't get a frame

int CRC_buf[5]={0};
int DATA_OUT[7] = {0}; 

//ADC function
float fun_ACP1 (void){
  RCC_ADC1_EN = ON;
  RCC_ADC_DIV = 00;
  ADC_ADON = ON;

  ADC_CONT = 0;
  ADC_SQ1 = ON;
  ADC_SMP1 = 0x03;
  ADC_SWSTART = ON;
  return ADC_DATA;
}

//Timer function
void SysTick_Handler(void) {
  k+=1;
}

//CRC function
u16 ModRTU_CRC(int buf[], int len) {
  u16 crc = 0xFFFF;
  for (int pos = 0; pos < len; pos++) {
    crc ^= buf[pos];
    for (int i = 8; i != 0; i--) {
      if ((crc & 0x0001) != 0) {
        crc >>= 1;
        crc ^= 0xA001;
      }
      else
        crc >>= 1;
    }
  }
  // �������, ��� ������� � ������� ����� �������� �������, ����������� �������������� (��� ���������������)
  return crc;
}

void main(void)
{ 
  
//*****************************RCC**********************************************
//Here you can give the tact of periphery  
  RCC_GPIOA_EN = ON;
  RCC_UART1_EN = ON;
  // RCC_GPIOC_EN = ON;
  
//*****************************systick******************************************  

  STK_EN = ON;
  STK_IRQ = ON;
  STK_REL = 999999;  
  
//*****************************ACP**********************************************  

  
//*****************************GPIO********************************************* 
//Here you can settings the ports
  //GPIO_CFG_PIN01(GPIOA, GPIO_ANALOG); //ACP
  GPIO_CFG_PIN10(GPIOA, GPIO_IN_PU); //Recive
  GPIO_CFG_PIN09(GPIOA, GPIO_AF_PP_PD_M); //Transmit
  
  // Analog Output
  
  // GPIO_CFG_PIN13(GPIOC, GPIO_OUT_PP_PU_M);

//*****************************USART********************************************
  USART_UE       = ON; //UART is on
  
  //How you need change BRR 
  USART_BRR = 0x341; // (APB1+BRR/2)/BRR
  //OR YOU CAN USE THE MANTISSA and FRACTION
  //USART_DIV_Fraction = 0x01;
  //USART_DIV_Mantissa = 0x341; // (APB1+BRR/2)/BRR
  
  //settings of CR1
  USART_RXNEIE   = ON; //���������� �� ������ �����
  USART_RE       = ON; //Recive is ON
  USART_TE       = ON; //Transmit is ON
 
  while (1){ 
    // calculation u
    u=3.3*fun_ACP1()/4095;
    
    //frame is get 
    //��������� ������. ����� ����� ���������� Flag_Pack=1. 
    //Flag_Pack ��������� � 0, ����� ����� ����� ���������
    if (USART_RXNE == ON && Flag_Pack == 0) {    
      DATA1[count] = USART_DR;
      count+=1;      
      if (count == 8) {
        count = 0;
        Flag_Pack = 1;
      }
    }
    
    //Create package
    //���������� ��������� � ������������ ������ � ������ DATA_OUT 
    DATA_OUT[0] = 0x01;                         //Adress
    DATA_OUT[1] = 0x03;                         //Command
    DATA_OUT[2] = 0x02;                         //����� ����
    DATA_OUT[3] = (int)u & 0xFF00;          //�������� �������� (������� ����)
    DATA_OUT[4] = (int)u & 0x00FF;            //�������� �������� (������� ����)
    //��������� ��� ���������� CRC 
    int CRC_buf1[5] = {DATA_OUT[0],DATA_OUT[1],DATA_OUT[2],DATA_OUT[3],DATA_OUT[4]};
      
    DATA_OUT[5] = ModRTU_CRC(CRC_buf1, 5) & 0x00FF;             //CRC 16. ������ ��� ������������ 2 �����
    DATA_OUT[6] = (ModRTU_CRC(CRC_buf1, 5) & 0xFF00) >> 8;

    // frame is sending 
    // ����� �������� ���������, Flag_Pack=0, ������ ����� ��������� ����� ������
    if (USART_TC == 1 && Flag_Pack == 1) {
      USART_DR = DATA_OUT[count_DR];     
      count_DR+=1;
      if (count_DR == 7) {
        count_DR = 0;
        Flag_Pack = 0;
      }
    }
  }
    
// u1=100*exp(u);
// if (u<=3.0)
//      {
//      for( int i =0; i<=100*exp(u); i++){}
//      GPIO_OUT(GPIOC) ^= GPIO_PIN13;
//      GPIO_BIT_SET(GPIOC) = GPIO_PIN13;
//      for( int i =0; i<=1*exp(u); i++){}
//      GPIO_BIT_RST(GPIOC) = GPIO_PIN13;
//      for( int i =0; i<=1*exp(u); i++){}
//      }
    

  
//  GPIO_CFG_PIN13(GPIOC, GPIO_OUT_PP_PU_M); // ������������ 14 ���� ����� �
//  GPIO_CFG_PIN14(GPIOC, GPIO_IN_PD);    // ������������ 15 ���� ����� �
//  while(1){
//   if(GPIO_IN(GPIOC) & 0x4000)  // ��������� ����� � �������� IDR (�� ����� �����) � ����������� �����,
     // ���� ����� 1, �� ���������� ����
//   {
//     for( int i =0; i<=100000; i++){}   // ������� ��� ������� ����������
//     GPIO_OUT(GPIOC) ^= GPIO_PIN13;     // ���� ����������
//   }
//  }
}
