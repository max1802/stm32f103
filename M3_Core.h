#ifdef __cplusplus
  extern "C" {
#endif
#define  M3                                                     //Core M3
#define  u8     unsigned char                                   //0...255
#define  s8     signed char                                     //-128...127
#define  u16    unsigned short                                  //0...65535
#define  s16    signed short                                    //-32768...2767
#define  u32    unsigned int                                    //0...4294967295
#define  s32    signed int                                      //-2147483647...2147483647
#define  u64    unsigned long long                              //0...18446744073709551615
#define  s64    signed long long                                //-9223372036854775807...9223372036854775807
#define  f32    float                                           //�1.18e-38 to �3.40e38 (exp 8bit (-126...127), mant 23bit)
#define  f64    double                                          //�2.23e-308 to �1.8e308 (exp 11bit (-1022...1023), mant 52bit)
/**************************************************************************************************/
#define ON                      ((u8)0x01)
#define OFF                     ((u8)0x00)
/************************************* Core M3 memory map *****************************************/
#define FLASH_BASE              ((u32) 0x08000000)              //FLASH base address
#define OB_BASE                 ((u32) 0x1FFFF800)              //Option byte base address
#define SRAM_BASE               ((u32) 0x20000000)              //SRAM base address
#define APB1_BASE               ((u32) 0x40000000)              //APB1 base address 
#define APB2_BASE               ((u32) 0x40010000)              //APB2 base address
#define SDIO_BASE               ((u32) 0x40018000)              //SDIO base address
#define AHB_BASE                ((u32) 0x40020000)              //AHB base address
#define ETH_BASE                ((u32) 0x40028000)              //ETH base address
#define USBOTG_BASE             ((u32) 0x50000000)              //USB OTG base address 
#define FSMC_BANK1              ((u32) 0x60000000)              //FSMC base address Bank 1
#define FSMC_BANK2              ((u32) 0x70000000)              //FSMC base address Bank 2
#define FSMC_BANK3              ((u32) 0x80000000)              //FSMC base address Bank 3
#define FSMC_BANK4              ((u32) 0x90000000)              //FSMC base address Bank 4
#define FSMC_BASE               ((u32) 0xA0000000)              //FSMC base address 
#define DWT_BASE                ((u32) 0xE0001000)              //Data Watchpoint and Trace unit Space Base Address
#define STK_BASE                ((u32) 0xE000E010)              //SysTick Base Address
#define NVIC_BASE               ((u32) 0xE000E100)              //NVIC Base Address
#define SCB_BASE                ((u32) 0xE000ED00)              //System Control Block Base Address
#define MPU_BASE                ((u32) 0xE000ED90)              //Memory protection unit Base Address
#define DBGMCU_BASE             ((u32) 0xE0042000)              //DBGMCU Base Address
/*********************************** Peripheral memory map ****************************************/
#define DMA1_BASE               (AHB_BASE + 0x0000)
#define DMA2_BASE               (AHB_BASE + 0x0400)
#define RCC_BASE                (AHB_BASE + 0x1000)
#define FLASH_R_BASE            (AHB_BASE + 0x2000)
#define CRC_BASE                (AHB_BASE + 0x3000)
#define TIM2_BASE               (APB1_BASE + 0x0000)
#define TIM3_BASE               (APB1_BASE + 0x0400)
#define TIM4_BASE               (APB1_BASE + 0x0800)
#define TIM5_BASE               (APB1_BASE + 0x0C00)
#define TIM6_BASE               (APB1_BASE + 0x1000)
#define TIM7_BASE               (APB1_BASE + 0x1400)
#define TIM12_BASE              (APB1_BASE + 0x1800)
#define TIM13_BASE              (APB1_BASE + 0x1C00)
#define TIM14_BASE              (APB1_BASE + 0x2000)
#define RTC_BASE                (APB1_BASE + 0x2800)
#define WWDG_BASE               (APB1_BASE + 0x2C00)
#define IWDG_BASE               (APB1_BASE + 0x3000)
#define SPI2_BASE               (APB1_BASE + 0x3800)
#define SPI3_BASE               (APB1_BASE + 0x3C00)
#define UART2_BASE              (APB1_BASE + 0x4400)
#define UART3_BASE              (APB1_BASE + 0x4800)
#define UART4_BASE              (APB1_BASE + 0x4C00)
#define UART5_BASE              (APB1_BASE + 0x5000)
#define I2C1_BASE               (APB1_BASE + 0x5400)
#define I2C2_BASE               (APB1_BASE + 0x5800)
#define USB_BASE                (APB1_BASE + 0x5C00)
#define USB_RAM_BASE            (APB1_BASE + 0x6000)
#define CAN1_BASE               (APB1_BASE + 0x6400)
#define CAN2_BASE               (APB1_BASE + 0x6800)
#define BKP_BASE                (APB1_BASE + 0x6C04)
#define PWR_BASE                (APB1_BASE + 0x7000)
#define DAC_BASE                (APB1_BASE + 0x7400)
#define CEC_BASE                (APB1_BASE + 0x7800)
#define AFIO_BASE               (APB2_BASE + 0x0000)
#define EXTI_BASE               (APB2_BASE + 0x0400)
#define GPIOA_BASE              (APB2_BASE + 0x0800)
#define GPIOB_BASE              (APB2_BASE + 0x0C00)
#define GPIOC_BASE              (APB2_BASE + 0x1000)
#define GPIOD_BASE              (APB2_BASE + 0x1400)
#define GPIOE_BASE              (APB2_BASE + 0x1800)
#define GPIOF_BASE              (APB2_BASE + 0x1C00)
#define GPIOG_BASE              (APB2_BASE + 0x2000)
#define ADC1_BASE               (APB2_BASE + 0x2400)
#define ADC2_BASE               (APB2_BASE + 0x2800)
#define TIM1_BASE               (APB2_BASE + 0x2C00)
#define SPI1_BASE               (APB2_BASE + 0x3000)
#define TIM8_BASE               (APB2_BASE + 0x3400)
#define UART1_BASE              (APB2_BASE + 0x3800)
#define ADC3_BASE               (APB2_BASE + 0x3C00)
#define TIM15_BASE              (APB2_BASE + 0x4000)
#define TIM16_BASE              (APB2_BASE + 0x4400)
#define TIM17_BASE              (APB2_BASE + 0x4800)
#define TIM9_BASE               (APB2_BASE + 0x4C00)
#define TIM10_BASE              (APB2_BASE + 0x5000)
#define TIM11_BASE              (APB2_BASE + 0x5400)
#define ETH_MAC_BASE            (ETH_BASE + 0x0000)
#define ETH_MMC_BASE            (ETH_BASE + 0x0100)
#define ETH_PTP_BASE            (ETH_BASE + 0x0700)
#define ETH_DMA_BASE            (ETH_BASE + 0x1000)
#define USBOTG_CORE_BASE        (USBOTG_BASE + 0x0000)
#define USBOTG_HOST_BASE        (USBOTG_BASE + 0x0400)
#define USBOTG_DEVICE_BASE      (USBOTG_BASE + 0x0800)
#define USBOTG_POWER_BASE       (USBOTG_BASE + 0x0E00)
#define USBOTG_EP0_BASE         (USBOTG_BASE + 0x1000)
#define USBOTG_EP1_BASE         (USBOTG_BASE + 0x2000)
#define USBOTG_EP2_BASE         (USBOTG_BASE + 0x3000)
#define USBOTG_EP3_BASE         (USBOTG_BASE + 0x4000)
#define USBOTG_EP4_BASE         (USBOTG_BASE + 0x5000)
#define USBOTG_EP5_BASE         (USBOTG_BASE + 0x6000)
#define USBOTG_EP6_BASE         (USBOTG_BASE + 0x7000)
#define USBOTG_EP7_BASE         (USBOTG_BASE + 0x8000)
#define USBOTG_DFIFO_BASE       (USBOTG_BASE + 0x20000)
/****************************************** IRQ ***************************************************/ 
typedef enum
{
  WWDG_IRQ                      = 0,  //Window WatchDog Interrupt
  PVD_IRQ                       = 1,  //PVD through EXTI Line detection Interrupt
  TAMPER_IRQ                    = 2,  //Tamper Interrupt
  RTC_IRQ                       = 3,  //RTC global Interrupt
  FLASH_IRQ                     = 4,  //FLASH global Interrupt
  RCC_IRQ                       = 5,  //RCC global Interrupt
  EXTI0_IRQ                     = 6,  //EXTI Line0 Interrupt
  EXTI1_IRQ                     = 7,  //EXTI Line1 Interrupt
  EXTI2_IRQ                     = 8,  //EXTI Line2 Interrupt
  EXTI3_IRQ                     = 9,  //EXTI Line3 Interrupt
  EXTI4_IRQ                     = 10, //EXTI Line4 Interrupt
  DMA1_CH1_IRQ                  = 11, //DMA1 Channel 1 global Interrupt
  DMA1_CH2_IRQ                  = 12, //DMA1 Channel 2 global Interrupt
  DMA1_CH3_IRQ                  = 13, //DMA1 Channel 3 global Interrupt
  DMA1_CH4_IRQ                  = 14, //DMA1 Channel 4 global Interrupt
  DMA1_CH5_IRQ                  = 15, //DMA1 Channel 5 global Interrupt
  DMA1_CH6_IRQ                  = 16, //DMA1 Channel 6 global Interrupt
  DMA1_CH7_IRQ                  = 17, //DMA1 Channel 7 global Interrupt
  ADC1_2_IRQ                    = 18, //ADC1 and ADC2 global Interrupt
  USB_HP_CAN1_TX_IRQ            = 19, //USB Device High Priority or CAN1 TX Interrupts
  USB_LP_CAN1_RX0_IRQ           = 20, //USB Device Low Priority or CAN1 RX Interrupts 
  CAN1_RX1_IRQ                  = 21, //CAN1 RX1 Interrupt
  CAN1_SCE_IRQ                  = 22, //CAN1 SCE Interrupt
  EXTI9_5_IRQ                   = 23, //External Line[9:5] Interrupt
  TIM1_BRK_IRQ                  = 24, //TIM1 Break Interrupt
  TIM1_UP_IRQ                   = 25, //TIM1 Update Interrupt 
  TIM1_TRG_COM_IRQ              = 26, //TIM1 Trigger and Commutation Interrupt
  TIM1_CC_IRQ                   = 27, //TIM1 Capture Compare Interrupt
  TIM2_IRQ                      = 28, //TIM2 global Interrupt
  TIM3_IRQ                      = 29, //TIM3 global Interrupt
  TIM4_IRQ                      = 30, //TIM4 global Interrupt
  I2C1_EV_IRQ                   = 31, //I2C1 Event Interrupt
  I2C1_ER_IRQ                   = 32, //I2C1 Error Interrupt
  I2C2_EV_IRQ                   = 33, //I2C2 Event Interrupt
  I2C2_ER_IRQ                   = 34, //I2C2 Error Interrupt
  SPI1_IRQ                      = 35, //SPI1 global Interrupt
  SPI2_IRQ                      = 36, //SPI2 global Interrupt
  UART1_IRQ                     = 37, //USART1 global Interrupt
  UART2_IRQ                     = 38, //USART2 global Interrupt
  UART3_IRQ                     = 39, //USART3 global Interrupt
  EXTI15_10_IRQ                 = 40, //External Line[15:10] Interrupts
  RTC_Alarm_IRQ                 = 41, //RTC Alarm through EXTI Line Interrupt
  USBWakeUp_IRQ                 = 42, //USB Device WakeUp from suspend through EXTI Line Interrupt
  TIM8_BRK_TIM12_IRQ            = 43, //TIM8 Break Interrupt and TIM12 global Interrupt
  TIM8_UP_TIM13_IRQ             = 44, //TIM8 Update Interrupt and TIM13 global Interrupt
  TIM8_TRG_COM_TIM14_IRQ        = 45, //TIM8 Trigger and Commutation Interrupt and TIM14 global interrupt
  TIM8_CC_IRQ                   = 46, //TIM8 Capture Compare Interrupt
  ADC3_IRQ                      = 47, //ADC3 global Interrupt
  FSMC_IRQ                      = 48, //FSMC global Interrupt
  SDIO_IRQ                      = 49, //SDIO global Interrupt
  TIM5_IRQ                      = 50, //TIM5 global Interrupt
  SPI3_IRQ                      = 51, //SPI3 global Interrupt
  UART4_IRQ                     = 52, //UART4 global Interrupt
  UART5_IRQ                     = 53, //UART5 global Interrupt
  TIM6_IRQ                      = 54, //TIM6 global Interrupt
  TIM7_IRQ                      = 55, //TIM7 global Interrupt
  DMA2_CH1_IRQ                  = 56, //DMA2 Channel 1 global Interrupt
  DMA2_CH2_IRQ                  = 57, //DMA2 Channel 2 global Interrupt
  DMA2_CH3_IRQ                  = 58, //DMA2 Channel 3 global Interrupt
  DMA2_CH4_IRQ                  = 59, //DMA2 Channel 4 global Interrupt
  DMA2_CH5_IRQ                  = 60, //DMA2 Channel 5 global Interrupt
  ETH_IRQ                       = 61, //Ethernet global Interrupt
  ETH_WKUP_IRQ                  = 62, //Ethernet Wakeup through EXTI line Interrupt
  CAN2_TX_IRQ                   = 63, //CAN2 TX Interrupt
  CAN2_RX0_IRQ                  = 64, //CAN2 RX0 Interrupt
  CAN2_RX1_IRQ                  = 65, //CAN2 RX1 Interrupt
  CAN2_SCE_IRQ                  = 66, //CAN2 SCE Interrupt
  OTG_FS_IRQ                    = 67, //USB OTG FS global Interrupt
}IRQ_Type;
/***************************************** SysTick ************************************************/
typedef struct
{
  struct
  {
    unsigned ENABLE     : 1;  //Counter enable
    unsigned TICKINT    : 1;  //SysTick exception request enable
    unsigned CLKSOURCE  : 1;  //Clock source selection
    unsigned RSVD0      : 13; //Reserved
    unsigned COUNTFLAG  : 1;  //Returns 1 if timer counted to 0 since last time this was read
    unsigned RSVD1      : 15; //Reserved
  }volatile CSR;              //SysTick control and status register (STK_CSR)
  struct
  {
    unsigned RELOAD     : 24; //RELOAD value
    unsigned RSVD0      : 8;  //Reserved
  }volatile RVR;              //SysTick reload value register (STK_RVR)
  struct
  {
    unsigned CURRENT    : 24; //Current counter value
    unsigned RSVD0      : 8;  //Reserved
  }volatile CVR;              //SysTick current value register (STK_CVR)
  struct
  {
    unsigned TENMS      : 24; //Calibration value
    unsigned RSVD0      : 6;  //Reserved
    unsigned SKEW       : 1;  //SKEW flag
    unsigned NOREF      : 1;  //NOREF flag
  }volatile CALIB;            //SysTick calibration value register (STK_CALIB)
}STK_Type;
#define STK ((STK_Type*) STK_BASE)
#define STK_EN                  STK->CSR.ENABLE                 //Counter enable
#define STK_IRQ                 STK->CSR.TICKINT                //SysTick exception request enable
#define STK_CLK                 STK->CSR.CLKSOURCE              //Clock source selection
#define STK_FLG                 STK->CSR.COUNTFLAG              //Returns 1 if timer counted to 0 since last time this was read
#define STK_REL                 STK->RVR.RELOAD                 //RELOAD value
#define STK_CUR                 STK->CVR.CURRENT                //Current counter value
#define STK_TENMS               STK->CALIB.TENMS                //Calibration value
#define STK_SKEW                STK->CALIB.SKEW                 //SKEW flag
#define STK_NOREF               STK->CALIB.NOREF                //NOREF flag
/******************************************* SCB **************************************************/
typedef struct
{
  struct
  {
    unsigned REV                : 4;  //Indicates patch release: 0x0 = Patch 0
    unsigned PART               : 12; //Indicates part number: 0xC23 = Cortex-M3
    unsigned CONST              : 4;  //Reads as 0xF
    unsigned VAR                : 4;  //Indicates processor revision: 0x2 = Revision 2
    unsigned IMP                : 8;  //Indicates implementor: 0x41 = ARM
  }volatile CPUID;                    //CPUID Base Register
  struct
  {
    unsigned VECTACTIVE         : 9;  //Active ISR number field
    unsigned RSVD0              : 2;  //Reserved
    unsigned RETTOBASE          : 1;  //This bit is 1 when the set of all active exceptions minus the IPSR_current_exception yields the empty set
    unsigned VECTPENDING        : 10; //Pending ISR number field
    unsigned ISRPENDING         : 1;  //Interrupt pending flag
    unsigned ISRPREEMPT         : 1;  //It indicates that a pending interrupt becomes active in the next running cycle
    unsigned RSVD1              : 1;  //Reserved
    unsigned PENDSTCLR          : 1;  //Clear pending SysTick bit
    unsigned PENDSTSET          : 1;  //Set a pending SysTick bit
    unsigned PENDSVCLR          : 1;  //Clear pending pendSV bit
    unsigned PENDSVSET          : 1;  //Set pending pendSV bit
    unsigned RSVD2              : 2;  //Reserved
    unsigned NMIPENDSET         : 1;  //Set pending NMI bit
  }volatile ICSR;                     //Interrupt Control and State Register
  struct
  {
    unsigned RSVD0              : 7;  //Reserved
    unsigned TBLOFF             : 22; //Vector table base offset field
    unsigned TBLBASE            : 1;  //Table base is in Code (0) or RAM (1)
    unsigned RSVD1              : 2;  //Reserved
  }volatile VTOR;                     //Vector Table Offset Register
  //struct
  //{
  //  unsigned VECTRESET          : 1;  //System Reset bit
  //  unsigned VECTCLRACT         : 1;  //Clear active vector bit
  //  unsigned SYSRESETREQ        : 1;  //Causes a signal to be asserted to the outer system that indicates a reset is requested
  //  unsigned RSVD0              : 5;  //Reserved
  //  unsigned PRIGROUP           : 3;  //Interrupt priority grouping field
  //  unsigned RSVD1              : 4;  //Reserved
  //  unsigned ENDIANESS          : 1;  //Data endianness bit (1 = big endian, 0 = little endian)
  //  unsigned VECTKEY            : 16; //Register key. Writing to this register requires 0x5FA in the VECTKEY field. Otherwise the write value is ignored. Reads as 0xFA05
  //}volatile AIRCR;                    //Application Interrupt and Reset Control Register
  volatile u32 AIRCR;                    //Application Interrupt and Reset Control Register
  struct
  {
    unsigned RSVD0              : 1;  //Reserved
    unsigned SLPONEXIT          : 1;  //Sleep on exit when returning from Handler mode to Thread mode
    unsigned SLEEPDEEP          : 1;  //Sleep deep bit
    unsigned RSVD1              : 1;  //Reserved
    unsigned SEVONPEND          : 1;  //When enabled, this causes WFE to wake up when an interrupt moves from inactive to pended
    unsigned RSVD2              : 27; //Reserved
  }volatile SCR;                      //System Control Register
  struct
  {
    unsigned NBTHRDENA          : 1;  //When 0, default, It is only possible to enter Thread mode when returning from the last exception. When set to 1, Thread mode can be entered from any level in Handler mode by controlled return value.
    unsigned USETMPEND          : 1;  //If written as 1, enables user code to write the Software Trigger Interrupt register to trigger (pend) a Main exception, which is one associated with the Main stack pointer.
    unsigned RSVD0              : 1;  //Reserved
    unsigned UNALIGNTRP         : 1;  //Trap for unaligned access
    unsigned DIV0TRP            : 1;  //Trap on Divide by 0
    unsigned RSVD1              : 3;  //Reserved
    unsigned BFHFNMIGN          : 1;  //When enabled, this causes handlers running at priority -1 and -2 (Hard Fault, NMI, and FAULTMASK escalated handlers) to ignore Data Bus faults caused by load and store instructions.
    unsigned STKALIGN           : 1;  //1 = 8-byte, 1 = 4-byte
    unsigned RSVD2              : 22; //Reserved
  }volatile CCR;                      //Configuration Control Register
  struct
  {
    unsigned PRI4               : 8;  //Priority of system handler 4, memory management fault
    unsigned PRI5               : 8;  //Priority of system handler 5, bus fault
    unsigned PRI6               : 8;  //Priority of system handler 6, usage fault
    unsigned RSVD0              : 8;  //Reserved
  }volatile SHPR1;                    //System handler priority register 1 (SCB_SHPR1)
  struct
  {
    unsigned RSVD0              : 24; //Reserved
    unsigned PRI11              : 8;  //Priority of system handler 11, SVCall
  }volatile SHPR2;                    //System handler priority register 2 (SCB_SHPR2)
  struct
  {
    unsigned RSVD0              : 16; //Reserved
    unsigned PRI14              : 8;  //Priority of system handler 14, PendSV
    unsigned PRI15              : 8;  //Priority of system handler 15, SysTick exception
  }volatile SHPR3;                    //System handler priority register 3 (SCB_SHPR3)
  struct
  {
    unsigned MEMFAULT           : 1;  //Memory management fault exception active bit, reads as 1 if exception isactive
    unsigned BUSFAULT           : 1;  //Bus fault exception active bit, reads as 1 if exception is active
    unsigned RSVD0              : 1;  //Reserved
    unsigned USGFAULT           : 1;  //Usage fault exception active bit, reads as 1 if exception is active
    unsigned RSVD1              : 3;  //Reserved
    unsigned SVCALL             : 1;  //SVC call active bit, reads as 1 if SVC call is active
    unsigned MONITOR            : 1;  //Debug monitor active bit, reads as 1 if Debug monitor is active
    unsigned RSVD2              : 1;  //Reserved
    unsigned PENDSV             : 1;  //PendSV exception active bit, reads as 1 if exception is active
    unsigned SYSTCK             : 1;  //SysTick exception active bit, reads as 1 if exception is active
    unsigned USGFLTPND          : 1;  //Usage fault exception pending bit, reads as 1 if exception is pending
    unsigned MEMFLTPND          : 1;  //Memory management fault exception pending bit, reads as 1 if exception is pending
    unsigned BUSFLTPND          : 1;  //Bus fault exception pending bit, reads as 1 if exception is pending
    unsigned SVCALLPND          : 1;  //SVC call pending bit, reads as 1 if exception is pending
    unsigned MEMFAULTEN         : 1;  //Memory management fault enable bit, set to 1 to enable
    unsigned BUSFAULTEN         : 1;  //Bus fault enable bit, set to 1 to enable
    unsigned USGFAULTEN         : 1;  //Usage fault enable bit, set to 1 to enable
    unsigned RSVD3              : 13; //Reserved
  }volatile SHCSR;                    //System handler control and state register (SCB_SHCSR)
  struct
  {
    unsigned IACCVIOL           : 1;  //Instruction access violation flag
    unsigned DACCVIOL           : 1;  //Data access violation flag
    unsigned RSVD0              : 1;  //Reserved
    unsigned MUNSTKERR          : 1;  //Memory manager fault on unstacking for a return from exception
    unsigned MSTKERR            : 1;  //Memory manager fault on stacking for exception entry
    unsigned RSVD1              : 2;  //Reserved
    unsigned MMARVALID          : 1;  //Memory Management Fault Address Register (MMAR) valid flag
    unsigned IBUSERR            : 1;  //Instruction bus error
    unsigned PRECISERR          : 1;  //Precise data bus error
    unsigned IMPRESERR          : 1;  //Imprecise data bus error
    unsigned UNSTKERR           : 1;  //Bus fault on unstacking for a return from exception
    unsigned STKERR             : 1;  //Bus fault on stacking for exception entry
    unsigned RSVD2              : 2;  //Reserved
    unsigned BFARVALID          : 1;  //Bus Fault Address Register (BFAR) valid flag
    unsigned UNDEFINSTR         : 1;  //Undefined instruction usage fault
    unsigned INVSTATE           : 1;  //Invalid state usage fault
    unsigned INVPC              : 1;  //Invalid PC load usage fault, caused by an invalid PC load by EXC_RETURN
    unsigned NOCP               : 1;  //No coprocessor usage fault. The processor does not support coprocessor instructions
    unsigned RSVD3              : 4;  //Reserved
    unsigned UNALIGNED          : 1;  //Unaligned access usage fault
    unsigned DIVBYZERO          : 1;  //Divide by zero usage fault
    unsigned RSVD4              : 6;  //Reserved
  }volatile CFSR;                     //Configurable fault status register (SCB_CFSR)
  struct
  {
    unsigned DEBUG_VT           : 1;  //Reserved for Debug use
    unsigned FORCED             : 1;  //Forced hard fault
    unsigned RSVD0              : 28; //Reserved
    unsigned VECTTBL            : 1;  //Vector table hard fault
    unsigned RSVD1              : 1;  //Reserved
  }volatile HFSR;                     //Hard fault status register (SCB_HFSR)
  struct
  {
    unsigned HALTED             : 1;  //Halt request flag
    unsigned BKPT               : 1;  //BKPT flag
    unsigned DWTTRAP            : 1;  //Data Watchpoint and Trace (DWT) flag
    unsigned VCATCH             : 1;  //Vector catch flag
    unsigned EXTERNAL           : 1;  //External debug request flag
    unsigned RSVD0              : 27; //Reserved
  }volatile DFSR;                     //Debug Fault Status Register(SCB_DFSR)
  volatile u32 MMFAR;                 //Memory Manage Fault Address Register(SCB_MMFAR)
  volatile u32 BFAR;                  //Bus Fault Address Register(SCB_BFAR)
  volatile u32 AFSR;                  //Auxiliary Fault Status Register(SCB_AFSR)
  volatile u32 ID_PFR0;               //Processor Feature Register 0(SCB_ID_PFR0)
  volatile u32 ID_PFR1;               //Processor Feature Register 1(SCB_ID_PFR1)
  volatile u32 ID_DFR0;               //Debug Features Register 0(SCB_ID_DFR0)
  volatile u32 ID_AFR0;               //Auxiliary Features Register 0(SCB_ID_AFR0)
  volatile u32 ID_MMFR0;              //Memory Model Feature Register 0(SCB_ID_MMFR0)
  volatile u32 ID_MMFR1;              //Memory Model Feature Register 1(SCB_ID_MMFR1)
  volatile u32 ID_MMFR2;              //Memory Model Feature Register 2(SCB_ID_MMFR2)
  volatile u32 ID_MMFR3;              //Memory Model Feature Register 3(SCB_ID_MMFR3)
  volatile u32 ID_ISAR0;              //Instruction Set Attributes Register 0(SCB_ID_ISAR0)
  volatile u32 ID_ISAR1;              //Instruction Set Attributes Register 1(SCB_ID_ISAR1)
  volatile u32 ID_ISAR2;              //Instruction Set Attributes Register 2(SCB_ID_ISAR2)
  volatile u32 ID_ISAR3;              //Instruction Set Attributes Register 3(SCB_ID_ISAR3)
  volatile u32 ID_ISAR4;              //Instruction Set Attributes Register 4(SCB_ID_ISAR4)
  volatile u32 RSVD0[5];              //Reserved
  volatile u32 CPACR;                 //Coprocessor Access Control Register(SCB_CPACR)
  volatile u32 RSVD1[93];             //Reserved
  struct
  {
    unsigned INTID              : 9;  //Interrupt ID field
    unsigned RSVD0              : 23; //Reserved
  }volatile STIR;                     //Software Triggered Interrupt Register(SCB_STIR)
}SCB_Type;
#define SCB ((SCB_Type*) SCB_BASE)
#define SCB_REV                 SCB->CPUID.REV                  //Indicates patch release: 0x0 = Patch 0
#define SCB_PART                SCB->CPUID.PART                 //Indicates part number: 0xC23 = Cortex-M3
#define SCB_CONST               SCB->CPUID.CONST                //Reads as 0xF
#define SCB_VAR                 SCB->CPUID.VAR                  //Indicates processor revision: 0x2 = Revision 2
#define SCB_IMP                 SCB->CPUID.IMP                  //Indicates implementor: 0x41 = ARM
#define SCB_VECTACTIVE          SCB->ICSR.VECTACTIVE            //Active ISR number field
#define SCB_RETTOBASE           SCB->ICSR.RETTOBASE             //This bit is 1 when the set of all active exceptions minus the IPSR_current_exception yields the empty set
#define SCB_VECTPENDING         SCB->ICSR.VECTPENDING           //Pending ISR number field
#define SCB_ISRPENDING          SCB->ICSR.ISRPENDING            //Interrupt pending flag
#define SCB_ISRPREEMPT          SCB->ICSR.ISRPREEMPT            //It indicates that a pending interrupt becomes active in the next running cycle
#define SCB_PENDSTCLR           SCB->ICSR.PENDSTCLR             //Clear pending SysTick bit
#define SCB_PENDSTSET           SCB->ICSR.PENDSTSET             //Set a pending SysTick bit
#define SCB_PENDSVCLR           SCB->ICSR.PENDSVCLR             //Clear pending pendSV bit
#define SCB_PENDSVSET           SCB->ICSR.PENDSVSET             //Set pending pendSV bit
#define SCB_NMIPENDSET          SCB->ICSR.NMIPENDSET            //Set pending NMI bit
#define SCB_TBLOFF              SCB->VTOR.TBLOFF                //Vector table base offset field
#define SCB_TBLBASE             SCB->VTOR.TBLBASE               //Table base is in Code (0) or RAM (1)
//#define SCB_VECTRESET           SCB->AIRCR.VECTRESET            //System Reset bit
//#define SCB_VECTCLRACT          SCB->AIRCR.VECTCLRACT           //Clear active vector bit
//#define SCB_SYSRESETREQ         SCB->AIRCR.SYSRESETREQ          //Causes a signal to be asserted to the outer system that indicates a reset is requested
//#define SCB_PRIGROUP            SCB->AIRCR.PRIGROUP             //Interrupt priority grouping field
//#define SCB_ENDIANESS           SCB->AIRCR.ENDIANESS            //Data endianness bit (1 = big endian, 0 = little endian)
//#define SCB_VECTKEY             SCB->AIRCR.VECTKEY              //Register key. Writing to this register requires 0x5FA in the VECTKEY field. Otherwise the write value is ignored. Reads as 0xFA05
#define SCB_SLPONEXIT           SCB->SCR.SLPONEXIT              //Sleep on exit when returning from Handler mode to Thread mode
#define SCB_SLEEPDEEP           SCB->SCR.SLEEPDEEP              //Sleep deep bit
#define SCB_SEVONPEND           SCB->SCR.SEVONPEND              //When enabled, this causes WFE to wake up when an interrupt moves from inactive to pended
#define SCB_NBTHRDENA           SCB->CCR.NBTHRDENA              //When 0, default, It is only possible to enter Thread mode when returning from the last exception. When set to 1, Thread mode can be entered from any level in Handler mode by controlled return value.
#define SCB_USETMPEND           SCB->CCR.USETMPEND              //If written as 1, enables user code to write the Software Trigger Interrupt register to trigger (pend) a Main exception, which is one associated with the Main stack pointer.
#define SCB_UNALIGNTRP          SCB->CCR.UNALIGNTRP             //Trap for unaligned access
#define SCB_DIV0TRP             SCB->CCR.DIV0TRP                //Trap on Divide by 0
#define SCB_BFHFNMIGN           SCB->CCR.BFHFNMIGN              //When enabled, this causes handlers running at priority -1 and -2 (Hard Fault, NMI, and FAULTMASK escalated handlers) to ignore Data Bus faults caused by load and store instructions.
#define SCB_STKALIGN            SCB->CCR.STKALIGN               //1 = 8-byte, 1 = 4-byte
#define SCB_PRI4                SCB->SHPR1.PRI4                 //Priority of system handler 4, memory management fault
#define SCB_PRI5                SCB->SHPR1.PRI5                 //Priority of system handler 5, bus fault
#define SCB_PRI6                SCB->SHPR1.PRI6                 //Priority of system handler 6, usage fault
#define SCB_PRI11               SCB->SHPR2.PRI11                //Priority of system handler 11, SVCall
#define SCB_PRI14               SCB->SHPR3.PRI14                //Priority of system handler 14, PendSV
#define SCB_PRI15               SCB->SHPR3.PRI15                //Priority of system handler 15, SysTick exception
#define SCB_MEMFAULT            SCB->SHCSR.MEMFAULT             //Memory management fault exception active bit, reads as 1 if exception isactive
#define SCB_BUSFAULT            SCB->SHCSR.BUSFAULT             //Bus fault exception active bit, reads as 1 if exception is active
#define SCB_USGFAULT            SCB->SHCSR.USGFAULT             //Usage fault exception active bit, reads as 1 if exception is active
#define SCB_SVCALL              SCB->SHCSR.SVCALL               //SVC call active bit, reads as 1 if SVC call is active
#define SCB_MONITOR             SCB->SHCSR.MONITOR              //Debug monitor active bit, reads as 1 if Debug monitor is active
#define SCB_PENDSV              SCB->SHCSR.PENDSV               //PendSV exception active bit, reads as 1 if exception is active
#define SCB_SYSTCK              SCB->SHCSR.SYSTCK               //SysTick exception active bit, reads as 1 if exception is active
#define SCB_USGFLTPND           SCB->SHCSR.USGFLTPND            //Usage fault exception pending bit, reads as 1 if exception is pending
#define SCB_MEMFLTPND           SCB->SHCSR.MEMFLTPND            //Memory management fault exception pending bit, reads as 1 if exception is pending
#define SCB_BUSFLTPND           SCB->SHCSR.BUSFLTPND            //Bus fault exception pending bit, reads as 1 if exception is pending
#define SCB_SVCALLPND           SCB->SHCSR.SVCALLPND            //SVC call pending bit, reads as 1 if exception is pending
#define SCB_MEMFAULTEN          SCB->SHCSR.MEMFAULTEN           //Memory management fault enable bit, set to 1 to enable
#define SCB_BUSFAULTEN          SCB->SHCSR.BUSFAULTEN           //Bus fault enable bit, set to 1 to enable
#define SCB_USGFAULTEN          SCB->SHCSR.USGFAULTEN           //Usage fault enable bit, set to 1 to enable
#define SCB_IACCVIOL            SCB->CFSR.IACCVIOL              //Instruction access violation flag
#define SCB_DACCVIOL            SCB->CFSR.DACCVIOL              //Data access violation flag
#define SCB_MUNSTKERR           SCB->CFSR.MUNSTKERR             //Memory manager fault on unstacking for a return from exception
#define SCB_MSTKERR             SCB->CFSR.MSTKERR               //Memory manager fault on stacking for exception entry
#define SCB_MMARVALID           SCB->CFSR.MMARVALID             //Memory Management Fault Address Register (MMAR) valid flag
#define SCB_IBUSERR             SCB->CFSR.IBUSERR               //Instruction bus error
#define SCB_PRECISERR           SCB->CFSR.PRECISERR             //Precise data bus error
#define SCB_IMPRESERR           SCB->CFSR.IMPRESERR             //Imprecise data bus error
#define SCB_UNSTKERR            SCB->CFSR.UNSTKERR              //Bus fault on unstacking for a return from exception
#define SCB_STKERR              SCB->CFSR.STKERR                //Bus fault on stacking for exception entry
#define SCB_BFARVALID           SCB->CFSR.BFARVALID             //Bus Fault Address Register (BFAR) valid flag
#define SCB_UNDEFINSTR          SCB->CFSR.UNDEFINSTR            //Undefined instruction usage fault
#define SCB_INVSTATE            SCB->CFSR.INVSTATE              //Invalid state usage fault
#define SCB_INVPC               SCB->CFSR.INVPC                 //Invalid PC load usage fault, caused by an invalid PC load by EXC_RETURN
#define SCB_NOCP                SCB->CFSR.NOCP                  //No coprocessor usage fault. The processor does not support coprocessor instructions
#define SCB_UNALIGNED           SCB->CFSR.UNALIGNED             //Unaligned access usage fault
#define SCB_DIVBYZERO           SCB->CFSR.DIVBYZERO             //Divide by zero usage fault
#define SCB_DEBUG_VT            SCB->HFSR.DEBUG_VT              //Reserved for Debug use
#define SCB_FORCED              SCB->HFSR.FORCED                //Forced hard fault
#define SCB_VECTTBL             SCB->HFSR.VECTTBL               //Vector table hard fault
#define SCB_HALTED              SCB->DFSR.HALTED                //Halt request flag
#define SCB_BKPT                SCB->DFSR.BKPT                  //BKPT flag
#define SCB_DWTTRAP             SCB->DFSR.DWTTRAP               //Data Watchpoint and Trace (DWT) flag
#define SCB_VCATCH              SCB->DFSR.VCATCH                //Vector catch flag
#define SCB_EXTERNAL            SCB->DFSR.EXTERNAL              //External debug request flag
#define SCB_MMFAR               SCB->MMFAR                      //Memory Manage Fault Address Register(SCB_MMFAR)
#define SCB_BFAR                SCB->BFAR                       //Bus Fault Address Register(SCB_BFAR)
#define SCB_AFSR                SCB->AFSR                       //Auxiliary Fault Status Register(SCB_AFSR)
#define SCB_ID_PFR0             SCB->ID_PFR0                    //Processor Feature Register 0(SCB_ID_PFR0)
#define SCB_ID_PFR1             SCB->ID_PFR1                    //Processor Feature Register 1(SCB_ID_PFR1)
#define SCB_ID_DFR0             SCB->ID_DFR0                    //Debug Features Register 0(SCB_ID_DFR0)
#define SCB_ID_AFR0             SCB->ID_AFR0                    //Auxiliary Features Register 0(SCB_ID_AFR0)
#define SCB_ID_MMFR0            SCB->ID_MMFR0                   //Memory Model Feature Register 0(SCB_ID_MMFR0)
#define SCB_ID_MMFR1            SCB->ID_MMFR1                   //Memory Model Feature Register 1(SCB_ID_MMFR1)
#define SCB_ID_MMFR2            SCB->ID_MMFR2                   //Memory Model Feature Register 2(SCB_ID_MMFR2)
#define SCB_ID_MMFR3            SCB->ID_MMFR3                   //Memory Model Feature Register 3(SCB_ID_MMFR3)
#define SCB_ID_ISAR0            SCB->ID_ISAR0                   //Instruction Set Attributes Register 0(SCB_ID_ISAR0)
#define SCB_ID_ISAR1            SCB->ID_ISAR1                   //Instruction Set Attributes Register 1(SCB_ID_ISAR1)
#define SCB_ID_ISAR2            SCB->ID_ISAR2                   //Instruction Set Attributes Register 2(SCB_ID_ISAR2)
#define SCB_ID_ISAR3            SCB->ID_ISAR3                   //Instruction Set Attributes Register 3(SCB_ID_ISAR3)
#define SCB_ID_ISAR4            SCB->ID_ISAR4                   //Instruction Set Attributes Register 4(SCB_ID_ISAR4)
#define SCB_CPACR               SCB->CPACR                      //Coprocessor Access Control Register(SCB_CPACR)
#define SCB_INTID               SCB->STIR.INTID                 //Interrupt ID field
/******************************************* MPU **************************************************/
typedef struct
{
  struct
  {
    unsigned SEPARATE   : 1;  //Because the processor core uses only a unified MPU, SEPARATE is always 0
    unsigned RSVD0      : 7;  //Reserved
    unsigned DREGION    : 8;  //Number of supported MPU regions field
    unsigned IREGION    : 8;  //Because the processor core uses only a unified MPU, IREGION always contains 0x00
    unsigned RSVD1      : 8;  //Reserved
  }volatile TR;               //MPU Type Register
  struct
  {
    unsigned ENABLE     : 1;  //MPU enable bit
    unsigned HFNMIENA   : 1;  //This bit enables the MPU when in Hard Fault, NMI, and FAULTMASK escalated handlers
    unsigned PRIVDEFENA : 1;  //This bit enables the default memory map for privileged access, as a background region, when the MPU is enabled
    unsigned RSVD0      : 29; //Reserved
  }volatile CR;               //MPU Control Register
  struct
  {
    unsigned REGION     : 8;  //Region select field
    unsigned RSVD0      : 24; //Reserved
  }volatile RNR;              //MPU Region Number Register
  struct
  {
    struct
    {
      unsigned REGION     : 4;  //MPU region override field
      unsigned VALID      : 1;  //MPU Region Number valid bit
      unsigned ADDR       : 27; //Region base address field
    }volatile RBAR;             //MPU Region Base Address Register
    struct
    {
      unsigned ENABLE     : 1;  //Region enable bit
      unsigned SIZE       : 5;  //MPU Protection Region Size Field
      unsigned RSVD0      : 2;  //Reserved
      unsigned SRD        : 8;  //Sub-Region Disable (SRD) field
      unsigned B          : 1;  //Bufferable bit
      unsigned C          : 1;  //Cacheable bit
      unsigned S          : 1;  //Shareable bit
      unsigned TEX        : 3;  //Type extension field
      unsigned RSVD1      : 2;  //Reserved
      unsigned AP         : 3;  //Data access permission field
      unsigned RSVD2      : 1;  //Reserved
      unsigned XN         : 1;  //Instruction access disable bit
      unsigned RSVD3      : 3;  //Reserved
    }volatile RASR;             //MPU Region Attribute and Size Register
  }ALIAS[4];
}MPU_Type;
#define MPU ((MPU_Type*) MPU_BASE)
#define MPU_SEPARATE            MPU->TR.SEPARATE                //Because the processor core uses only a unified MPU, SEPARATE is always 0
#define MPU_DREGION             MPU->TR.DREGION                 //Number of supported MPU regions field
#define MPU_IREGION             MPU->TR.IREGION                 //Because the processor core uses only a unified MPU, IREGION always contains 0x00
#define MPU_EN                  MPU->CR.ENABLE                  //MPU enable bit
#define MPU_HFNMIENA            MPU->CR.HFNMIENA                //This bit enables the MPU when in Hard Fault, NMI, and FAULTMASK escalated handlers
#define MPU_PRIVDEFENA          MPU->CR.PRIVDEFENA              //This bit enables the default memory map for privileged access, as a background region, when the MPU is enabled
#define MPU_REG                 MPU->RNR.REGION                 //Region select field
#define MPU_REGION(CH)          MPU->ALIAS[CH].RBAR.REGION      //MPU region override field
#define MPU_VALID(CH)           MPU->ALIAS[CH].RBAR.VALID       //MPU Region Number valid bit
#define MPU_ADDR(CH)            MPU->ALIAS[CH].RBAR.ADDR        //Region base address field
#define MPU_ENABLE(CH)          MPU->ALIAS[CH].RASR.ENABLE      //Region enable bit
#define MPU_SIZE(CH)            MPU->ALIAS[CH].RASR.SIZE        //MPU Protection Region Size Field
#define MPU_SRD(CH)             MPU->ALIAS[CH].RASR.SRD         //Sub-Region Disable (SRD) field
#define MPU_B(CH)               MPU->ALIAS[CH].RASR.B           //Bufferable bit
#define MPU_C(CH)               MPU->ALIAS[CH].RASR.C           //Cacheable bit
#define MPU_S(CH)               MPU->ALIAS[CH].RASR.S           //Shareable bit
#define MPU_TEX(CH)             MPU->ALIAS[CH].RASR.TEX         //Type extension field
#define MPU_AP(CH)              MPU->ALIAS[CH].RASR.AP          //Data access permission field
#define MPU_XN(CH)              MPU->ALIAS[CH].RASR.XN          //Instruction access disable bit
/******************************************* DWT **************************************************/
typedef struct
{
  struct
  {
    unsigned CYCCNTENA  : 1;  //Enable the CYCCNT counter
    unsigned POSTPRESET : 4;  //Reload value for POSTCNT, bits [8:5], post-scalar counter
    unsigned POSTCNT    : 4;  //Post-scalar counter for CYCTAP
    unsigned CYCTAP     : 1;  //Selects a tap on the DWT_CYCCNT register
    unsigned SYNCTAP    : 2;  //Feeds a synchronization pulse to the ITM SYNCENA control
    unsigned PCSAMPLEEN : 1;  //Enables PC Sampling event
    unsigned RSVD0      : 3;  //Reserved
    unsigned EXCTRCENA  : 1;  //Enables Interrupt event tracing
    unsigned CPIEVTENA  : 1;  //Enables CPI count event
    unsigned EXCEVTENA  : 1;  //Enables Interrupt overhead event
    unsigned SLEEPEVTEN : 1;  //Enables Sleep count event
    unsigned LSUEVTENA  : 1;  //Enables LSU count event
    unsigned FOLDEVTEN  : 1;  //Enables Folded instruction count event
    unsigned CYCEVTEN   : 1;  //Enables Cycle count event
    unsigned RSVD1      : 5;  //Reserved
    unsigned NUMCOMP    : 4;  //Number of comparators field
  }volatile CTRL;             //DWT Control Register
  volatile u32 CYCCNT;        //Cycle Count Register
  struct
  {
    unsigned CPICNT     : 8;  //Current CPI counter value
    unsigned RSVD0      : 24; //Reserved
  }volatile CPICNT;           //CPI Count Register
  struct
  {
    unsigned EXCCNT     : 8;  //Current interrupt overhead counter value
    unsigned RSVD0      : 24; //Reserved
  }volatile EXCCNT;           //Exception Overhead Count Register
  struct
  {
    unsigned SLEEPCNT   : 8;  //Sleep counter
    unsigned RSVD0      : 24; //Reserved
  }volatile SLEEPCNT;         //Sleep Count Register
  struct
  {
    unsigned LSUCNT     : 8;  //LSU counter
    unsigned RSVD0      : 24; //Reserved
  }volatile LSUCNT;           //LSU Count Register
  struct
  {
    unsigned FOLDCNT    : 8;  //This counts the total number folded instructions
    unsigned RSVD0      : 24; //Reserved
  }volatile FOLDCNT;          //Folded-instruction Count Register
  volatile u32 PCSR;          //Program Counter Sample Register
  struct
  {
    volatile u32 COMP;          //Comparator Register
    struct
    {
      unsigned MASK       : 4;  //Mask on data address when matching against COMP
      unsigned RSVD0      : 28; //Reserved
    }volatile MASK;             //Mask Register
    struct
    {
      unsigned FUNCTION   : 4;  //FUNCTION settings
      unsigned RSVD0      : 1;  //Reserved
      unsigned EMITRANGE  : 1;  //Emit range field
      unsigned RSVD1      : 1;  //Reserved
      unsigned CYCMATCH   : 1;  //Only available in comparator 0
      unsigned DATAVMATCH : 1;  //This bit is only available in comparator 1
      unsigned LNK1ENA    : 1;  //Read-only
      unsigned DATAVSIZE  : 2;  //Defines the size of the data in the COMP register that is to be matched
      unsigned DATAVADDR0 : 4;  //Identity of a linked address comparator for data value matching when DATAVMATCH = 1
      unsigned DATAVADDR1 : 4;  //Identity of a second linked address comparator for data value matching when DATAVMATCH = 1 and LNK1ENA = 1.
      unsigned RSVD2      : 4;  //Reserved
      unsigned MATCHED    : 1;  //This bit is set when the comparator matches, and indicates that the operation defined by FUNCTION has occurred since this bit was last read
      unsigned RSVD3      : 7;  //Reserved
    }volatile FUNC;             //Function Register
  }COUNT[4];
  volatile u32 RSVD0[0x03DE];
  volatile u32 PID4;            //Peripheral identification registers
  volatile u32 PID5;            //Peripheral identification registers
  volatile u32 PID6;            //Peripheral identification registers
  volatile u32 PID7;            //Peripheral identification registers
  volatile u32 PID0;            //Peripheral identification registers
  volatile u32 PID1;            //Peripheral identification registers
  volatile u32 PID2;            //Peripheral identification registers
  volatile u32 PID3;            //Peripheral identification registers
  volatile u32 CID0;            //Peripheral identification registers
  volatile u32 CID1;            //Peripheral identification registers
  volatile u32 CID2;            //Peripheral identification registers
  volatile u32 CID3;            //Peripheral identification registers
}DWT_Type;
#define DWT ((DWT_Type*) DWT_BASE)
#define DWT_CYCCNTENA           DWT->CTRL.CYCCNTENA             //Enable the CYCCNT counter
#define DWT_POSTPRESET          DWT->CTRL.POSTPRESET            //Reload value for POSTCNT, bits [8:5], post-scalar counter
#define DWT_POSTCNT             DWT->CTRL.POSTCNT               //Post-scalar counter for CYCTAP
#define DWT_CYCTAP              DWT->CTRL.CYCTAP                //Selects a tap on the DWT_CYCCNT register
#define DWT_SYNCTAP             DWT->CTRL.SYNCTAP               //Feeds a synchronization pulse to the ITM SYNCENA control
#define DWT_PCSAMPLEEN          DWT->CTRL.PCSAMPLEEN            //Enables PC Sampling event
#define DWT_EXCTRCENA           DWT->CTRL.EXCTRCENA             //Enables Interrupt event tracing
#define DWT_CPIEVTENA           DWT->CTRL.CPIEVTENA             //Enables CPI count event
#define DWT_EXCEVTENA           DWT->CTRL.EXCEVTENA             //Enables Interrupt overhead event
#define DWT_SLEEPEVTEN          DWT->CTRL.SLEEPEVTEN            //Enables Sleep count event
#define DWT_LSUEVTENA           DWT->CTRL.LSUEVTENA             //Enables LSU count event
#define DWT_FOLDEVTEN           DWT->CTRL.FOLDEVTEN             //Enables Folded instruction count event
#define DWT_CYCEVTEN            DWT->CTRL.CYCEVTEN              //Enables Cycle count event
#define DWT_NUMCOMP             DWT->CTRL.NUMCOMP               //Number of comparators field
#define DWT_CYCCNT              DWT->CYCCNT                     //Cycle Count Register
#define DWT_CPICNT              DWT->CPICNT.CPICNT              //Current CPI counter value
#define DWT_EXCCNT              DWT->EXCCNT.EXCCNT              //Current interrupt overhead counter value
#define DWT_SLEEPCNT            DWT->SLEEPCNT.SLEEPCNT          //Sleep counter
#define DWT_LSUCNT              DWT->LSUCNT.LSUCNT              //LSU counter
#define DWT_FOLDCNT             DWT->FOLDCNT.FOLDCNT            //This counts the total number folded instructions
#define DWT_PCSR                DWT->PCSR                       //Program Counter Sample Register
#define DWT_COMP(CH)            DWT->COUNT[CH].COMP             //Comparator Register0
#define DWT_MASK(CH)            DWT->COUNT[CH].MASK.MASK        //Mask on data address when matching against COMP
#define DWT_FUNCTION(CH)        DWT->COUNT[CH].FUNC.FUNCTION    //FUNCTION settings
#define DWT_EMITRANGE(CH)       DWT->COUNT[CH].FUNC.EMITRANGE   //Emit range field
#define DWT_CYCMATCH(CH)        DWT->COUNT[CH].FUNC.CYCMATCH    //Only available in comparator 0
#define DWT_DATAVMATCH(CH)      DWT->COUNT[CH].FUNC.DATAVMATCH  //This bit is only available in comparator 1
#define DWT_LNK1ENA(CH)         DWT->COUNT[CH].FUNC.LNK1ENA     //Read-only
#define DWT_DATAVSIZE(CH)       DWT->COUNT[CH].FUNC.DATAVSIZE   //Defines the size of the data in the COMP register that is to be matched
#define DWT_DATAVADDR0(CH)      DWT->COUNT[CH].FUNC.DATAVADDR0  //Identity of a linked address comparator for data value matching when DATAVMATCH = 1
#define DWT_DATAVADDR1(CH)      DWT->COUNT[CH].FUNC.DATAVADDR1  //Identity of a second linked address comparator for data value matching when DATAVMATCH = 1 and LNK1ENA = 1.
#define DWT_MATCHED(CH)         DWT->COUNT[CH].FUNC.MATCHED     //This bit is set when the comparator matches, and indicates that the operation defined by FUNCTION has occurred since this bit was last read
#define DWT_PID4                DWT->PID4                       //Peripheral identification registers
#define DWT_PID5                DWT->PID5                       //Peripheral identification registers
#define DWT_PID6                DWT->PID6                       //Peripheral identification registers
#define DWT_PID7                DWT->PID7                       //Peripheral identification registers
#define DWT_PID0                DWT->PID0                       //Peripheral identification registers
#define DWT_PID1                DWT->PID1                       //Peripheral identification registers
#define DWT_PID2                DWT->PID2                       //Peripheral identification registers
#define DWT_PID3                DWT->PID3                       //Peripheral identification registers
#define DWT_CID0                DWT->CID0                       //Peripheral identification registers
#define DWT_CID1                DWT->CID1                       //Peripheral identification registers
#define DWT_CID2                DWT->CID2                       //Peripheral identification registers
#define DWT_CID3                DWT->CID3                       //Peripheral identification registers
/******************************************* NVIC *************************************************/
typedef struct
{
  volatile u32 ISER[8];         //Interrupt set-enable registers (NVIC_ISERx)
  volatile u32 RSVD0[24];       //Reserved
  volatile u32 ICER[8];         //Interrupt clear-enable registers (NVIC_ICERx)
  volatile u32 RSVD1[24];       //Reserved
  volatile u32 ISPR[8];         //Interrupt set-pending registers (NVIC_ISPRx)
  volatile u32 RSVD2[24];       //Reserved
  volatile u32 ICPR[8];         //Interrupt clear-pending registers (NVIC_ICPRx)
  volatile u32 RSVD3[24];       //Reserved
  volatile u32 IABR[8];         //Interrupt active bit registers (NVIC_IABRx)
  volatile u32 RSVD4[56];       //Reserved
  volatile u32 IPR[240];        //Interrupt priority bit registers (NVIC_IPRx)
  volatile u32 RSVD5[644];      //Reserved
  volatile u32 STIR;            //Software Trigger Interrupt Register (NVIC_STIR)
}NVIC_Type;
#define NVIC ((NVIC_Type*) NVIC_BASE)
/******************************************* RCC **************************************************/
typedef struct
{
  struct
  {
    unsigned HSION      : 1;  //Internal high-speed clock enable
    unsigned HSIRDY     : 1;  //Internal high-speed clock ready flag
    unsigned RSVD0      : 1;  //Reserved
    unsigned HSITRIM    : 5;  //Internal high-speed clock trimming
    unsigned HSICAL     : 8;  //Internal high-speed clock calibration
    unsigned HSEON      : 1;  //External high-speed clock enable
    unsigned HSERDY     : 1;  //External high-speed clock ready flag
    unsigned HSEBYP     : 1;  //External high-speed clock bypass
    unsigned CSSON      : 1;  //Clock security system enable
    unsigned RSVD1      : 4;  //Reserved
    unsigned PLLON      : 1;  //PLL enable
    unsigned PLLRDY     : 1;  //PLL clock ready flag
    unsigned PLL2ON     : 1;  //PLL2 enable
    unsigned PLL2RDY    : 1;  //PLL2 clock ready flag
    unsigned PLL3ON     : 1;  //PLL3 enable
    unsigned PLL3RDY    : 1;  //PLL3 clock ready flag
    unsigned RSVD2      : 2;  //Reserved
  }volatile CR;               //Clock control register (RCC_CR)
  struct
  {
    unsigned SW         : 2;  //System clock switch
    unsigned SWS        : 2;  //System clock switch status
    unsigned HPRE       : 4;  //AHB prescaler
    unsigned PPRE1      : 3;  //APB low-speed prescaler (APB1)
    unsigned PPRE2      : 3;  //APB high-speed prescaler (APB2)
    unsigned ADCPRE     : 2;  //ADC prescaler
    unsigned PLLSRC     : 1;  //PLL entry clock source
    unsigned PLLXTPRE   : 1;  //HSE divider for PLL entry
    unsigned PLLMUL     : 4;  //PLL multiplication factor
    unsigned USBPRE     : 1;  //USB prescaler
    unsigned RSVD0      : 1;  //Reserved
    unsigned MCO        : 4;  //Microcontroller clock output
    unsigned RSVD1      : 4;  //Reserved
  }volatile CFGR;             //Clock configuration register (RCC_CFGR)
  struct
  {
    unsigned LSIRDYF    : 1;  //LSI ready interrupt flag
    unsigned LSERDYF    : 1;  //LSE ready interrupt flag
    unsigned HSIRDYF    : 1;  //HSI ready interrupt flag
    unsigned HSERDYF    : 1;  //HSE ready interrupt flag
    unsigned PLLRDYF    : 1;  //PLL ready interrupt flag
    unsigned PLL2RDYF   : 1;  //PLL2 ready interrupt flag
    unsigned PLL3RDYF   : 1;  //PLL3 ready interrupt flag
    unsigned CSSF       : 1;  //Clock security system interrupt flag
    unsigned LSIRDYIE   : 1;  //LSI ready interrupt enable
    unsigned LSERDYIE   : 1;  //LSE ready interrupt enable
    unsigned HSIRDYIE   : 1;  //HSI ready interrupt enable
    unsigned HSERDYIE   : 1;  //HSE ready interrupt enable
    unsigned PLLRDYIE   : 1;  //PLL ready interrupt enable
    unsigned PLL2RDYIE  : 1;  //PLL2 ready interrupt enable
    unsigned PLL3RDYIE  : 1;  //PLL3 ready interrupt enable
    unsigned RSVD1      : 1;  //Reserved
    unsigned LSIRDYC    : 1;  //LSI ready interrupt clear
    unsigned LSERDYC    : 1;  //LSE ready interrupt clear
    unsigned HSIRDYC    : 1;  //HSI ready interrupt clear
    unsigned HSERDYC    : 1;  //HSE ready interrupt clear
    unsigned PLLRDYC    : 1;  //PLL ready interrupt clear
    unsigned PLL2RDYC   : 1;  //PLL2 ready interrupt clear
    unsigned PLL3RDYC   : 1;  //PLL3 ready interrupt clear
    unsigned CSSC       : 1;  //Clock security system interrupt clear
    unsigned RSVD3      : 8;  //Reserved
  }volatile CIR;              //Clock interrupt register (RCC_CIR)
  struct
  {
    unsigned AFIORST    : 1;  //Alternate function IO reset
    unsigned RSVD0      : 1;  //Reserved
    unsigned IOPARST    : 1;  //IO port A reset
    unsigned IOPBRST    : 1;  //IO port B reset
    unsigned IOPCRST    : 1;  //IO port C reset
    unsigned IOPDRST    : 1;  //IO port D reset
    unsigned IOPERST    : 1;  //IO port E reset
    unsigned IOPFRST    : 1;  //IO port F reset
    unsigned IOPGRST    : 1;  //IO port G reset
    unsigned ADC1RST    : 1;  //ADC 1 interface reset
    unsigned ADC2RST    : 1;  //ADC 2 interface reset
    unsigned TIM1RST    : 1;  //TIM1 timer reset
    unsigned SPI1RST    : 1;  //SPI1 reset
    unsigned TIM8RST    : 1;  //TIM8 timer reset
    unsigned UART1RST   : 1;  //USART1 reset
    unsigned ADC3RST    : 1;  //ADC 3 interface reset
    unsigned RSVD3      : 3;  //Reserved
    unsigned TIM9RST    : 1;  //TIM9 timer reset
    unsigned TIM10RST   : 1;  //TIM10 timer reset
    unsigned TIM11RST   : 1;  //TIM11 timer reset
    unsigned RSVD4      : 10; //Reserved
  }volatile APB2RSTR;         //APB2 peripheral reset register (RCC_APB2RSTR)
  struct
  {
    unsigned TIM2RST    : 1;  //TIM2 timer reset
    unsigned TIM3RST    : 1;  //TIM3 timer reset
    unsigned TIM4RST    : 1;  //TIM4 timer reset
    unsigned TIM5RST    : 1;  //TIM5 timer reset
    unsigned TIM6RST    : 1;  //TIM6 timer reset
    unsigned TIM7RST    : 1;  //TIM7 timer reset
    unsigned TIM12RST   : 1;  //TIM12 timer reset
    unsigned TIM13RST   : 1;  //TIM13 timer reset
    unsigned TIM14RST   : 1;  //TIM14 timer reset
    unsigned RSVD0      : 2;  //Reserved
    unsigned WWDGRST    : 1;  //Window watchdog reset
    unsigned RSVD1      : 2;  //Reserved
    unsigned SPI2RST    : 1;  //SPI2 reset
    unsigned SPI3RST    : 1;  //SPI3 reset
    unsigned RSVD2      : 1;  //Reserved
    unsigned UART2RST   : 1;  //USART2 reset
    unsigned UART3RST   : 1;  //USART3 reset
    unsigned UART4RST   : 1;  //UART4 reset
    unsigned UART5RST   : 1;  //UART5 reset
    unsigned I2C1RST    : 1;  //I2C1 reset
    unsigned I2C2RST    : 1;  //I2C2 reset
    unsigned USBRST     : 1;  //USB reset
    unsigned RSVD3      : 1;  //Reserved
    unsigned CANRST     : 1;  //CAN reset
    unsigned CAN2RST    : 1;  //CAN2 reset
    unsigned BKPRST     : 1;  //Backup interface reset
    unsigned PWRRST     : 1;  //Power interface reset
    unsigned DACRST     : 1;  //DAC interface reset
    unsigned RSVD5      : 2;  //Reserved
  }volatile APB1RSTR;         //APB1 peripheral reset register (RCC_APB1RSTR)
  struct
  {
    unsigned DMA1EN     : 1;  //DMA1 clock enable
    unsigned DMA2EN     : 1;  //DMA2 clock enable
    unsigned SRAMEN     : 1;  //SRAM interface clock enable
    unsigned RSVD0      : 1;  //Reserved
    unsigned FLITFEN    : 1;  //FLITF clock enable
    unsigned RSVD1      : 1;  //Reserved
    unsigned CRCEN      : 1;  //CRC clock enable
    unsigned RSVD2      : 1;  //Reserved
    unsigned FSMCEN     : 1;  //FSMCEN clock enable
    unsigned RSVD3      : 1;  //Reserved
    unsigned SDIOEN     : 1;  //SDIOEN clock enable
    unsigned RSVD4      : 1;  //Reserved
    unsigned OTGFSEN    : 1;  //USB OTG FS clock enable
    unsigned RSVD5      : 1;  //Reserved
    unsigned ETHMACEN   : 1;  //Ethernet MAC clock enable
    unsigned ETHMACTXEN : 1;  //Ethernet MAC TX clock enable
    unsigned ETHMACRXEN : 1;  //Ethernet MAC RX clock enable
    unsigned RSVD6      : 15; //Reserved
  }volatile AHBENR;           //AHB peripheral clock enable register (RCC_AHBENR)
  struct
  {
    unsigned AFIOEN     : 1;  //Alternate function IO clock enable
    unsigned RSVD0      : 1;  //Reserved
    unsigned IOPAEN     : 1;  //IO port A clock enable
    unsigned IOPBEN     : 1;  //IO port B clock enable
    unsigned IOPCEN     : 1;  //IO port C clock enable
    unsigned IOPDEN     : 1;  //IO port D clock enable
    unsigned IOPEEN     : 1;  //IO port E clock enable
    unsigned IOPFEN     : 1;  //IO port F clock enable
    unsigned IOPGEN     : 1;  //IO port G clock enable
    unsigned ADC1EN     : 1;  //ADC 1 interface clock enable
    unsigned ADC2EN     : 1;  //ADC 2 interface clock enable
    unsigned TIM1EN     : 1;  //TIM1 timer clock enable
    unsigned SPI1EN     : 1;  //SPI1 clock enable
    unsigned TIM8EN     : 1;  //TIM8 clock enable
    unsigned UART1EN    : 1;  //USART1 clock enable
    unsigned ADC3EN     : 1;  //ADC 3 interface clock enable
    unsigned RSVD3      : 3;  //Reserved
    unsigned TIM9EN     : 1;  //TIM9 timer clock enable
    unsigned TIM10EN    : 1;  //TIM10 timer clock enable
    unsigned TIM11EN    : 1;  //TIM11 timer clock enable
    unsigned RSVD4      : 10; //Reserved
  }volatile APB2ENR;          //APB2 peripheral clock enable register (RCC_APB2ENR)
  struct
  {
    unsigned TIM2EN     : 1;  //TIM2 timer clock enable
    unsigned TIM3EN     : 1;  //TIM3 timer clock enable
    unsigned TIM4EN     : 1;  //TIM4 timer clock enable
    unsigned TIM5EN     : 1;  //TIM5 timer clock enable
    unsigned TIM6EN     : 1;  //TIM6 timer clock enable
    unsigned TIM7EN     : 1;  //TIM7 timer clock enable
    unsigned TIM12EN    : 1;  //TIM12 timer clock enable
    unsigned TIM13EN    : 1;  //TIM13 timer clock enable
    unsigned TIM14EN    : 1;  //TIM14 timer clock enable
    unsigned RSVD0      : 2;  //Reserved
    unsigned WWDGEN     : 1;  //Window watchdog clock enable
    unsigned RSVD1      : 2;  //Reserved
    unsigned SPI2EN     : 1;  //SPI2 clock enable
    unsigned SPI3EN     : 1;  //SPI3 clock enable
    unsigned RSVD2      : 1;  //Reserved
    unsigned UART2EN    : 1;  //USART2 clock enable
    unsigned UART3EN    : 1;  //USART3 clock enable
    unsigned UART4EN    : 1;  //UART4 clock enable
    unsigned UART5EN    : 1;  //UART5 clock enable
    unsigned I2C1EN     : 1;  //I2C1 clock enable
    unsigned I2C2EN     : 1;  //I2C2 clock enable
    unsigned USBEN      : 1;  //USB clock enable
    unsigned RSVD3      : 1;  //Reserved
    unsigned CANEN      : 1;  //CAN clock enable
    unsigned CAN2EN     : 1;  //CAN2 clock enable
    unsigned BKPEN      : 1;  //Backup interface clock enable
    unsigned PWREN      : 1;  //Power interface clock enable
    unsigned DACEN      : 1;  //DAC interface clock enable
    unsigned RSVD5      : 2;  //Reserved
  }volatile APB1ENR;          //APB1 peripheral clock enable register (RCC_APB1ENR)
  struct
  {
    unsigned LSEON      : 1;  //External low-speed oscillator enable
    unsigned LSERDY     : 1;  //External low-speed oscillator ready
    unsigned LSEBYP     : 1;  //External low-speed oscillator bypass
    unsigned RSVD0      : 5;  //Reserved
    unsigned RTCSEL     : 2;  //RTC clock source selection
    unsigned RSVD1      : 5;  //Reserved
    unsigned RTCEN      : 1;  //RTC clock enable
    unsigned BDRST      : 1;  //Backup domain software reset
    unsigned RSVD2      : 15; //Reserved
  }volatile BDCR;             //Backup domain control register (RCC_BDCR)
  struct
  {
    unsigned LSION      : 1;  //Internal low-speed oscillator enable
    unsigned LSIRDY     : 1;  //Internal low-speed oscillator ready
    unsigned RSVD0      : 22; //Reserved
    unsigned RMVF       : 1;  //Remove reset flag
    unsigned RSVD1      : 1;  //Reserved
    unsigned PINRSTF    : 1;  //PIN reset flag
    unsigned PORRSTF    : 1;  //POR/PDR reset flag
    unsigned SFTRSTF    : 1;  //Software reset flag
    unsigned IWDGRSTF   : 1;  //Independent watchdog reset flag
    unsigned WWDGRSTF   : 1;  //Window watchdog reset flag
    unsigned LPWRRSTF   : 1;  //Low-power reset flag
  }volatile CSR;              //Control/status register (RCC_CSR)
  struct
  {
    unsigned RSVD0      : 12; //Reserved
    unsigned OTGFSRST   : 1;  //USB OTG FS reset
    unsigned RSVD1      : 1;  //Reserved
    unsigned ETHMACRST  : 1;  //Ethernet MAC reset
    unsigned RSVD2      : 17; //Reserved
  }volatile AHBRSTR;          //AHB peripheral clock reset register (RCC_AHBRSTR)
  struct
  {
    unsigned PREDIV1    : 4;  //PREDIV1 division factor
    unsigned PREDIV2    : 4;  //PREDIV2 division factor
    unsigned PLL2MUL    : 4;  //PLL2 Multiplication Factor
    unsigned PLL3MUL    : 4;  //PLL3 Multiplication Factor
    unsigned PREDIV1SRC : 1;  //PLL2 selected as PREDIV1 clock entry
    unsigned I2S2SRC    : 1;  //I2S2 clock source
    unsigned I2S3SRC    : 1;  //I2S3 clock source
    unsigned RSVD0      : 17; //Reserved
  }volatile CFGR2;            //Clock configuration register2 (RCC_CFGR2)
} RCC_Type;
#define RCC ((RCC_Type*) RCC_BASE)
#define RCC_HSI_EN              RCC->CR.HSION                   //Internal high-speed clock enable
#define RCC_HSI_RDY             RCC->CR.HSIRDY                  //Internal high-speed clock ready flag
#define RCC_HSI_CLK             RCC->CR.HSITRIM                 //Internal high-speed clock trimming
#define RCC_HSI_CAL             RCC->CR.HSICAL                  //Internal high-speed clock calibration
#define RCC_HSE_EN              RCC->CR.HSEON                   //External high-speed clock enable
#define RCC_HSE_RDY             RCC->CR.HSERDY                  //External high-speed clock ready flag
#define RCC_HSE_BYP             RCC->CR.HSEBYP                  //External high-speed clock bypass
#define RCC_CSS_EN              RCC->CR.CSSON                   //Clock security system enable
#define RCC_PLL_EN              RCC->CR.PLLON                   //PLL enable
#define RCC_PLL_RDY             RCC->CR.PLLRDY                  //PLL clock ready flag
#define RCC_PLL2_EN             RCC->CR.PLL2ON                  //PLL2 enable
#define RCC_PLL2_RDY            RCC->CR.PLL2RDY                 //PLL2 clock ready flag
#define RCC_PLL3_EN             RCC->CR.PLL3ON                  //PLL3 enable
#define RCC_PLL3_RDY            RCC->CR.PLL3RDY                 //PLL3 clock ready flag
#define RCC_SYSCLK              RCC->CFGR.SW                    //System clock switch
#define RCC_SYSCLK_FLG          RCC->CFGR.SWS                   //System clock switch status
#define RCC_AHB_DIV             RCC->CFGR.HPRE                  //AHB prescaler
#define RCC_APB1_DIV            RCC->CFGR.PPRE1                 //APB low-speed prescaler (APB1)
#define RCC_APB2_DIV            RCC->CFGR.PPRE2                 //APB high-speed prescaler (APB2)
#define RCC_ADC_DIV             RCC->CFGR.ADCPRE                //ADC prescaler
#define RCC_PLL_CLK             RCC->CFGR.PLLSRC                //PLL entry clock source
#define RCC_HSE_DIV             RCC->CFGR.PLLXTPRE              //HSE divider for PLL entry
#define RCC_PLL_MUX             RCC->CFGR.PLLMUL                //PLL multiplication factor
#define RCC_USB_DIV             RCC->CFGR.USBPRE                //USB prescaler
#define RCC_MCO_CLK             RCC->CFGR.MCO                   //Microcontroller clock output
#define RCC_LSI_IRQ_FLG         RCC->CIR.LSIRDYF                //LSI ready interrupt flag
#define RCC_LSE_IRQ_FLG         RCC->CIR.LSERDYF                //LSE ready interrupt flag
#define RCC_HSI_IRQ_FLG         RCC->CIR.HSIRDYF                //HSI ready interrupt flag
#define RCC_HSE_IRQ_FLG         RCC->CIR.HSERDYF                //HSE ready interrupt flag
#define RCC_PLL_IRQ_FLG         RCC->CIR.PLLRDYF                //PLL ready interrupt flag
#define RCC_PLL2_IRQ_FLG        RCC->CIR.PLL2RDYF               //PLL2 ready interrupt flag
#define RCC_PLL3_IRQ_FLG        RCC->CIR.PLL3RDYF               //PLL3 ready interrupt flag
#define RCC_CSS_IRQ_FLG         RCC->CIR.CSSF                   //Clock security system interrupt flag
#define RCC_LSI_IRQ             RCC->CIR.LSIRDYIE               //LSI ready interrupt enable
#define RCC_LSE_IRQ             RCC->CIR.LSERDYIE               //LSE ready interrupt enable
#define RCC_HSI_IRQ             RCC->CIR.HSIRDYIE               //HSI ready interrupt enable
#define RCC_HSE_IRQ             RCC->CIR.HSERDYIE               //HSE ready interrupt enable
#define RCC_PLL_IRQ             RCC->CIR.PLLRDYIE               //PLL ready interrupt enable
#define RCC_PLL2_IRQ            RCC->CIR.PLL2RDYIE              //PLL2 ready interrupt enable
#define RCC_PLL3_IRQ            RCC->CIR.PLL3RDYIE              //PLL3 ready interrupt enable
#define RCC_LSI_IRQ_CLR         RCC->CIR.LSIRDYC = ON           //LSI ready interrupt clear
#define RCC_LSE_IRQ_CLR         RCC->CIR.LSERDYC = ON           //LSE ready interrupt clear
#define RCC_HSI_IRQ_CLR         RCC->CIR.HSIRDYC = ON           //HSI ready interrupt clear
#define RCC_HSE_IRQ_CLR         RCC->CIR.HSERDYC = ON           //HSE ready interrupt clear
#define RCC_PLL_IRQ_CLR         RCC->CIR.PLLRDYC = ON           //PLL ready interrupt clear
#define RCC_PLL2_IRQ_CLR        RCC->CIR.PLL2RDYC = ON          //PLL2 ready interrupt clear
#define RCC_PLL3_IRQ_CLR        RCC->CIR.PLL3RDYC = ON          //PLL3 ready interrupt clear
#define RCC_CSS_IRQ_CLR         RCC->CIR.CSSC = ON              //Clock security system interrupt clear
#define RCC_AFIO_RST            RCC->APB2RSTR.AFIORST = ON      //Alternate function IO reset
#define RCC_GPIOA_RST           RCC->APB2RSTR.IOPARST = ON      //IO port A reset
#define RCC_GPIOB_RST           RCC->APB2RSTR.IOPBRST = ON      //IO port B reset
#define RCC_GPIOC_RST           RCC->APB2RSTR.IOPCRST = ON      //IO port C reset
#define RCC_GPIOD_RST           RCC->APB2RSTR.IOPDRST = ON      //IO port D reset
#define RCC_GPIOE_RST           RCC->APB2RSTR.IOPERST = ON      //IO port E reset
#define RCC_GPIOF_RST           RCC->APB2RSTR.IOPFRST = ON      //IO port F reset
#define RCC_GPIOG_RST           RCC->APB2RSTR.IOPGRST = ON      //IO port G reset
#define RCC_ADC1_RST            RCC->APB2RSTR.ADC1RST = ON      //ADC 1 interface reset
#define RCC_ADC2_RST            RCC->APB2RSTR.ADC2RST = ON      //ADC 2 interface reset
#define RCC_TIM1_RST            RCC->APB2RSTR.TIM1RST = ON      //TIM1 timer reset
#define RCC_SPI1_RST            RCC->APB2RSTR.SPI1RST = ON      //SPI1 reset
#define RCC_UART1_RST           RCC->APB2RSTR.UART1RST = ON     //USART1 reset
#define RCC_TIM8_RST            RCC->APB2RSTR.TIM8RST = ON      //TIM8 timer reset
#define RCC_ADC3_RST            RCC->APB2RSTR.ADC3RST = ON      //ADC 3 interface reset
#define RCC_TIM9_RST            RCC->APB2RSTR.TIM9RST = ON      //TIM9 timer reset
#define RCC_TIM10_RST           RCC->APB2RSTR.TIM10RST = ON     //TIM10 timer reset
#define RCC_TIM11_RST           RCC->APB2RSTR.TIM11RST = ON     //TIM11 timer reset
#define RCC_TIM2_RST            RCC->APB1RSTR.TIM2RST = ON      //TIM2 timer reset
#define RCC_TIM3_RST            RCC->APB1RSTR.TIM3RST = ON      //TIM3 timer reset
#define RCC_TIM4_RST            RCC->APB1RSTR.TIM4RST = ON      //TIM4 timer reset
#define RCC_TIM5_RST            RCC->APB1RSTR.TIM5RST = ON      //TIM5 timer reset
#define RCC_TIM6_RST            RCC->APB1RSTR.TIM6RST = ON      //TIM6 timer reset
#define RCC_TIM7_RST            RCC->APB1RSTR.TIM7RST = ON      //TIM7 timer reset
#define RCC_TIM12_RST           RCC->APB1RSTR.TIM12RST = ON     //TIM12 timer reset
#define RCC_TIM13_RST           RCC->APB1RSTR.TIM13RST = ON     //TIM13 timer reset
#define RCC_TIM14_RST           RCC->APB1RSTR.TIM14RST = ON     //TIM14 timer reset
#define RCC_WWDG_RST            RCC->APB1RSTR.WWDGRST = ON      //Window watchdog reset
#define RCC_SPI2_RST            RCC->APB1RSTR.SPI2RST = ON      //SPI2 reset
#define RCC_SPI3_RST            RCC->APB1RSTR.SPI3RST = ON      //SPI3 reset
#define RCC_UART2_RST           RCC->APB1RSTR.UART2RST = ON     //USART2 reset
#define RCC_UART3_RST           RCC->APB1RSTR.UART3RST = ON     //USART3 reset
#define RCC_UART4_RST           RCC->APB1RSTR.UART4RST = ON     //UART4 reset
#define RCC_UART5_RST           RCC->APB1RSTR.UART5RST = ON     //UART5 reset
#define RCC_I2C1_RST            RCC->APB1RSTR.I2C1RST = ON      //I2C1 reset
#define RCC_I2C2_RST            RCC->APB1RSTR.I2C2RST = ON      //I2C2 reset
#define RCC_USB_RST             RCC->APB1RSTR.USBRST = ON       //USB reset
#define RCC_CAN_RST             RCC->APB1RSTR.CANRST = ON       //CAN reset
#define RCC_CAN2_RST            RCC->APB1RSTR.CAN2RST = ON      //CAN2 reset
#define RCC_BKP_RST             RCC->APB1RSTR.BKPRST = ON       //Backup interface reset
#define RCC_PWR_RST             RCC->APB1RSTR.PWRRST = ON       //Power interface reset
#define RCC_DAC_RST             RCC->APB1RSTR.DACRST = ON       //DAC interface reset
#define RCC_DMA1_EN             RCC->AHBENR.DMA1EN              //DMA1 clock enable
#define RCC_DMA2_EN             RCC->AHBENR.DMA2EN              //DMA2 clock enable
#define RCC_SRAM_EN             RCC->AHBENR.SRAMEN              //SRAM interface clock enable
#define RCC_FLITF_EN            RCC->AHBENR.FLITFEN             //FLITF clock enable
#define RCC_CRC_EN              RCC->AHBENR.CRCEN               //CRC clock enable
#define RCC_FSMC_EN             RCC->AHBENR.FSMCEN              //FSMC clock enable
#define RCC_SDIO_EN             RCC->AHBENR.SDIOEN              //SDIO clock enable
#define RCC_OTGFS_EN            RCC->AHBENR.OTGFSEN             //USB OTG FS clock enable
#define RCC_ETH_EN              RCC->AHBENR.ETHMACEN            //Ethernet MAC clock enable
#define RCC_ETH_TX_EN           RCC->AHBENR.ETHMACTXEN          //Ethernet MAC TX clock enable
#define RCC_ETH_RX_EN           RCC->AHBENR.ETHMACRXEN          //Ethernet MAC RX clock enable
#define RCC_AFIO_EN             RCC->APB2ENR.AFIOEN             //Alternate function IO clock enable
#define RCC_GPIOA_EN            RCC->APB2ENR.IOPAEN             //IO port A clock enable
#define RCC_GPIOB_EN            RCC->APB2ENR.IOPBEN             //IO port B clock enable
#define RCC_GPIOC_EN            RCC->APB2ENR.IOPCEN             //IO port C clock enable
#define RCC_GPIOD_EN            RCC->APB2ENR.IOPDEN             //IO port D clock enable
#define RCC_GPIOE_EN            RCC->APB2ENR.IOPEEN             //IO port E clock enable
#define RCC_GPIOF_EN            RCC->APB2ENR.IOPFEN             //IO port F clock enable
#define RCC_GPIOG_EN            RCC->APB2ENR.IOPGEN             //IO port G clock enable
#define RCC_ADC1_EN             RCC->APB2ENR.ADC1EN             //ADC 1 interface clock enable
#define RCC_ADC2_EN             RCC->APB2ENR.ADC2EN             //ADC 2 interface clock enable
#define RCC_TIM1_EN             RCC->APB2ENR.TIM1EN             //TIM1 timer clock enable
#define RCC_SPI1_EN             RCC->APB2ENR.SPI1EN             //SPI1 clock enable
#define RCC_TIM8_EN             RCC->APB2ENR.TIM8EN             //TIM8 timer clock enable
#define RCC_UART1_EN            RCC->APB2ENR.UART1EN            //USART1 clock enable
#define RCC_ADC3_EN             RCC->APB2ENR.ADC3EN             //ADC 3 interface clock enable
#define RCC_TIM9_EN             RCC->APB2ENR.TIM9EN             //TIM9 timer clock enable
#define RCC_TIM10_EN            RCC->APB2ENR.TIM10EN            //TIM10 timer clock enable
#define RCC_TIM11_EN            RCC->APB2ENR.TIM11EN            //TIM11 timer clock enable
#define RCC_TIM2_EN             RCC->APB1ENR.TIM2EN             //TIM2 timer clock enable
#define RCC_TIM3_EN             RCC->APB1ENR.TIM3EN             //TIM3 timer clock enable
#define RCC_TIM4_EN             RCC->APB1ENR.TIM4EN             //TIM4 timer clock enable
#define RCC_TIM5_EN             RCC->APB1ENR.TIM5EN             //TIM5 timer clock enable
#define RCC_TIM6_EN             RCC->APB1ENR.TIM6EN             //TIM6 timer clock enable
#define RCC_TIM7_EN             RCC->APB1ENR.TIM7EN             //TIM7 timer clock enable
#define RCC_TIM12_EN            RCC->APB1ENR.TIM12EN            //TIM12 timer clock enable
#define RCC_TIM13_EN            RCC->APB1ENR.TIM13EN            //TIM13 timer clock enable
#define RCC_TIM14_EN            RCC->APB1ENR.TIM14EN            //TIM14 timer clock enable
#define RCC_WWDG_EN             RCC->APB1ENR.WWDGEN             //Window watchdog clock enable
#define RCC_SPI2_EN             RCC->APB1ENR.SPI2EN             //SPI2 clock enable
#define RCC_SPI3_EN             RCC->APB1ENR.SPI3EN             //SPI3 clock enable
#define RCC_UART2_EN            RCC->APB1ENR.UART2EN            //USART2 clock enable
#define RCC_UART3_EN            RCC->APB1ENR.UART3EN            //USART3 clock enable
#define RCC_UART4_EN            RCC->APB1ENR.UART4EN            //UART4 clock enable
#define RCC_UART5_EN            RCC->APB1ENR.UART5EN            //UART5 clock enable
#define RCC_I2C1_EN             RCC->APB1ENR.I2C1EN             //I2C1 clock enable
#define RCC_I2C2_EN             RCC->APB1ENR.I2C2EN             //I2C2 clock enable
#define RCC_USB_EN              RCC->APB1ENR.USBEN              //USB clock enable
#define RCC_CAN_EN              RCC->APB1ENR.CANEN              //CAN clock enable
#define RCC_CAN2_EN             RCC->APB1ENR.CAN2EN             //CAN2 clock enable
#define RCC_BKP_EN              RCC->APB1ENR.BKPEN              //Backup interface clock enable
#define RCC_PWR_EN              RCC->APB1ENR.PWREN              //Power interface clock enable
#define RCC_DAC_EN              RCC->APB1ENR.DACEN              //DAC interface clock enable
#define RCC_LSE_EN              RCC->BDCR.LSEON                 //External low-speed oscillator enable
#define RCC_LSE_RDY             RCC->BDCR.LSERDY                //External low-speed oscillator ready
#define RCC_LSE_BYP             RCC->BDCR.LSEBYP                //External low-speed oscillator bypass
#define RCC_RTC_CLK             RCC->BDCR.RTCSEL                //RTC clock source selection
#define RCC_RTC_EN              RCC->BDCR.RTCEN                 //RTC clock enable
#define RCC_RTC_RST             RCC->BDCR.BDRST = ON            //Backup domain software reset
#define RCC_LSI_EN              RCC->CSR.LSION                  //Internal low-speed oscillator enable
#define RCC_LSI_RDY             RCC->CSR.LSIRDY                 //Internal low-speed oscillator ready
#define RCC_RMVRST_FLG          RCC->CSR.RMVF                   //Remove reset flag
#define RCC_PINRST_FLG          RCC->CSR.PINRSTF                //PIN reset flag
#define RCC_PORRST_FLG          RCC->CSR.PORRSTF                //POR/PDR reset flag
#define RCC_SFTRST_FLG          RCC->CSR.SFTRSTF                //Software reset flag
#define RCC_IWDGRST_FLG         RCC->CSR.IWDGRSTF               //Independent watchdog reset flag
#define RCC_WWDGRST_FLG         RCC->CSR.WWDGRSTF               //Window watchdog reset flag
#define RCC_LPWRRST_FLG         RCC->CSR.LPWRRSTF               //Low-power reset flag
#define RCC_OTG_RST             RCC->AHBRSTR.OTGFSRST           //USB OTG FS reset
#define RCC_ETH_RST             RCC->AHBRSTR.ETHMACRST          //Ethernet MAC reset
#define RCC_PREDIV1             RCC->CFGR2.PREDIV1              //PREDIV1 division factor
#define RCC_PREDIV2             RCC->CFGR2.PREDIV2              //PREDIV2 division factor
#define RCC_PLL2_MUX            RCC->CFGR2.PLL2MUL              //PLL2 Multiplication Factor
#define RCC_PLL3_MUX            RCC->CFGR2.PLL3MUL              //PLL3 Multiplication Factor
#define RCC_PREDIV1_SRC         RCC->CFGR2.PREDIV1SRC           //PLL2 selected as PREDIV1 clock entry
#define RCC_I2S2SRC             RCC->CFGR2.I2S2SRC              //I2S2 clock source
#define RCC_I2S3SRC             RCC->CFGR2.I2S3SRC              //I2S3 clock source
#define RCC_HSI_CLK_7360000     ((u8) 0x00)
#define RCC_HSI_CLK_7400000     ((u8) 0x01)
#define RCC_HSI_CLK_7440000     ((u8) 0x02)
#define RCC_HSI_CLK_7480000     ((u8) 0x03)
#define RCC_HSI_CLK_7520000     ((u8) 0x04)
#define RCC_HSI_CLK_7560000     ((u8) 0x05)
#define RCC_HSI_CLK_7600000     ((u8) 0x06)
#define RCC_HSI_CLK_7640000     ((u8) 0x07)
#define RCC_HSI_CLK_7680000     ((u8) 0x08)
#define RCC_HSI_CLK_7720000     ((u8) 0x09)
#define RCC_HSI_CLK_7760000     ((u8) 0x0A)
#define RCC_HSI_CLK_7800000     ((u8) 0x0B)
#define RCC_HSI_CLK_7840000     ((u8) 0x0C)
#define RCC_HSI_CLK_7880000     ((u8) 0x0D)
#define RCC_HSI_CLK_7920000     ((u8) 0x0E)
#define RCC_HSI_CLK_7960000     ((u8) 0x0F)
#define RCC_HSI_CLK_8000000     ((u8) 0x10)
#define RCC_HSI_CLK_8040000     ((u8) 0x11)
#define RCC_HSI_CLK_8080000     ((u8) 0x12)
#define RCC_HSI_CLK_8120000     ((u8) 0x13)
#define RCC_HSI_CLK_8160000     ((u8) 0x14)
#define RCC_HSI_CLK_8200000     ((u8) 0x15)
#define RCC_HSI_CLK_8240000     ((u8) 0x16)
#define RCC_HSI_CLK_8280000     ((u8) 0x17)
#define RCC_HSI_CLK_8320000     ((u8) 0x18)
#define RCC_HSI_CLK_8360000     ((u8) 0x19)
#define RCC_HSI_CLK_8400000     ((u8) 0x1A)
#define RCC_HSI_CLK_8440000     ((u8) 0x1B)
#define RCC_HSI_CLK_8480000     ((u8) 0x1C)
#define RCC_HSI_CLK_8520000     ((u8) 0x1D)
#define RCC_HSI_CLK_8560000     ((u8) 0x1E)
#define RCC_HSI_CLK_8600000     ((u8) 0x1F)
#define RCC_SYS_HSI             ((u8) 0x00) //HSI selected as system clock
#define RCC_SYS_HSE             ((u8) 0x01) //HSE selected as system clock
#define RCC_SYS_PLL             ((u8) 0x02) //PLL selected as system clock
#define RCC_AHB_DIV001          ((u8) 0x00) //SYSCLK not divided
#define RCC_AHB_DIV002          ((u8) 0x08) //SYSCLK divided by 2
#define RCC_AHB_DIV004          ((u8) 0x09) //SYSCLK divided by 4
#define RCC_AHB_DIV008          ((u8) 0x0A) //SYSCLK divided by 8
#define RCC_AHB_DIV016          ((u8) 0x0B) //SYSCLK divided by 16
#define RCC_AHB_DIV064          ((u8) 0x0C) //SYSCLK divided by 64
#define RCC_AHB_DIV128          ((u8) 0x0D) //SYSCLK divided by 128
#define RCC_AHB_DIV256          ((u8) 0x0E) //SYSCLK divided by 256
#define RCC_AHB_DIV512          ((u8) 0x0F) //SYSCLK divided by 512
#define RCC_APB_DIV01           ((u8) 0x00) //HCLK not divided
#define RCC_APB_DIV02           ((u8) 0x04) //HCLK divided by 2
#define RCC_APB_DIV04           ((u8) 0x05) //HCLK divided by 4
#define RCC_APB_DIV08           ((u8) 0x06) //HCLK divided by 8
#define RCC_APB_DIV16           ((u8) 0x07) //HCLK divided by 16
#define RCC_ADC_DIV2            ((u8) 0x00) //PCLK2 divided by 2
#define RCC_ADC_DIV4            ((u8) 0x01) //PCLK2 divided by 4
#define RCC_ADC_DIV6            ((u8) 0x02) //PCLK2 divided by 6
#define RCC_ADC_DIV8            ((u8) 0x03) //PCLK2 divided by 8
#define RCC_PLLSRC_HSI          ((u8) 0x00) //HSI oscillator clock/2 selected as PLL input clock
#define RCC_PLLSRC_HSE          ((u8) 0x01) //HSE oscillator clock selected as PLL input clock
#define RCC_HSE_DIV01           ((u8) 0x00) //HSE clock not divided
#define RCC_HSE_DIV02           ((u8) 0x01) //HSE clock divided by 2
#define RCC_PLL_MUX02           ((u8) 0x00) //PLL input clock x 2 (L, M, H and XL-density)
#define RCC_PLL_MUX03           ((u8) 0x01) //PLL input clock x 3 (L, M, H and XL-density)
#define RCC_PLL_MUX04           ((u8) 0x02) //PLL input clock x 4
#define RCC_PLL_MUX05           ((u8) 0x03) //PLL input clock x 5
#define RCC_PLL_MUX06           ((u8) 0x04) //PLL input clock x 6
#define RCC_PLL_MUX65           ((u8) 0x0D) //PLL input clock x 6.5 (Connectivity line devices)
#define RCC_PLL_MUX07           ((u8) 0x05) //PLL input clock x 7
#define RCC_PLL_MUX08           ((u8) 0x06) //PLL input clock x 8
#define RCC_PLL_MUX09           ((u8) 0x07) //PLL input clock x 9
#define RCC_PLL_MUX10           ((u8) 0x08) //PLL input clock x 10 (L, M, H and XL-density)
#define RCC_PLL_MUX11           ((u8) 0x09) //PLL input clock x 11 (L, M, H and XL-density)
#define RCC_PLL_MUX12           ((u8) 0x0A) //PLL input clock x 12 (L, M, H and XL-density)
#define RCC_PLL_MUX13           ((u8) 0x0B) //PLL input clock x 13 (L, M, H and XL-density)
#define RCC_PLL_MUX14           ((u8) 0x0C) //PLL input clock x 14 (L, M, H and XL-density)
#define RCC_PLL_MUX15           ((u8) 0x0D) //PLL input clock x 15 (L, M, H and XL-density)
#define RCC_PLL_MUX16           ((u8) 0x0E) //PLL input clock x 16 (L, M, H and XL-density)
#define RCC_USB_DIV15           ((u8) 0x00) //PLL clock is divided by 1.5 (L, M, H and XL-density)
#define RCC_USB_DIV10           ((u8) 0x01) //PLL clock is not divided (L, M, H and XL-density)
#define RCC_USBOTG_DIV3         ((u8) 0x00) //PLL VCO (2*PLLCLK) clock is divided by 3 (PLL must be configured to output 72 MHz) (Connectivity line devices)
#define RCC_USBOTG_DIV2         ((u8) 0x01) //PLL VCO (2*PLLCLK) clock is divided by 2 (PLL must be configured to output 48 MHz) (Connectivity line devices)
#define RCC_MCO_NOCLK           ((u8) 0x00) //No clock
#define RCC_MCO_SYSCLK          ((u8) 0x04) //System clock (SYSCLK) selected
#define RCC_MCO_HSICLK          ((u8) 0x05) //HSI clock selected
#define RCC_MCO_HSECLK          ((u8) 0x06) //HSE clock selected
#define RCC_MCO_PLLCLK          ((u8) 0x07) //PLL clock divided by 2 selected
#define RCC_MCO_PLL2CLK         ((u8) 0x08) //PLL2 clock selected (Connectivity line devices)
#define RCC_MCO_PLL3CLK         ((u8) 0x09) //PLL3 clock divided by 2 selected (Connectivity line devices)
#define RCC_MCO_EXTCLK          ((u8) 0x0A) //XT1 external 3-25 MHz oscillator clock selected (for Ethernet) (Connectivity line devices)
#define RCC_MCO_ETHCLK          ((u8) 0x0B) //PLL3 clock selected (for Ethernet) (Connectivity line devices)
#define RCC_RTC_NOCLK           ((u8) 0x00) //No clock
#define RCC_RTC_LSECLK          ((u8) 0x01) //LSE oscillator clock used as RTC clock10
#define RCC_RTC_LSICLK          ((u8) 0x02) //LSI oscillator clock used as RTC clock
#define RCC_RTC_HSECLK          ((u8) 0x03) //HSE oscillator clock divided by 128 used as RTC clock
#define RCC_DIV01               ((u8) 0x00) //PREDIV1 input clock not divided (Connectivity line devices)
#define RCC_DIV02               ((u8) 0x01) //PREDIV1 input clock divided by 2 (Connectivity line devices)
#define RCC_DIV03               ((u8) 0x02) //PREDIV1 input clock divided by 3 (Connectivity line devices)
#define RCC_DIV04               ((u8) 0x03) //PREDIV1 input clock divided by 4 (Connectivity line devices)
#define RCC_DIV05               ((u8) 0x04) //PREDIV1 input clock divided by 5 (Connectivity line devices)
#define RCC_DIV06               ((u8) 0x05) //PREDIV1 input clock divided by 6 (Connectivity line devices)
#define RCC_DIV07               ((u8) 0x06) //PREDIV1 input clock divided by 7 (Connectivity line devices)
#define RCC_DIV08               ((u8) 0x07) //PREDIV1 input clock divided by 8 (Connectivity line devices)
#define RCC_DIV09               ((u8) 0x08) //PREDIV1 input clock divided by 9 (Connectivity line devices)
#define RCC_DIV10               ((u8) 0x09) //PREDIV1 input clock divided by 10 (Connectivity line devices)
#define RCC_DIV11               ((u8) 0x0A) //PREDIV1 input clock divided by 11 (Connectivity line devices)
#define RCC_DIV12               ((u8) 0x0B) //PREDIV1 input clock divided by 12 (Connectivity line devices)
#define RCC_DIV13               ((u8) 0x0C) //PREDIV1 input clock divided by 13 (Connectivity line devices)
#define RCC_DIV14               ((u8) 0x0D) //PREDIV1 input clock divided by 14 (Connectivity line devices)
#define RCC_DIV15               ((u8) 0x0E) //PREDIV1 input clock divided by 15 (Connectivity line devices)
#define RCC_DIV16               ((u8) 0x0F) //PREDIV1 input clock divided by 16 (Connectivity line devices)
#define RCC_PLL23_MUX08         ((u8) 0x06) //PLL23 input clock x 8 (Connectivity line devices)
#define RCC_PLL23_MUX09         ((u8) 0x07) //PLL23 input clock x 9 (Connectivity line devices)
#define RCC_PLL23_MUX10         ((u8) 0x08) //PLL23 input clock x 10 (Connectivity line devices)
#define RCC_PLL23_MUX11         ((u8) 0x09) //PLL23 input clock x 11 (Connectivity line devices)
#define RCC_PLL23_MUX12         ((u8) 0x0A) //PLL23 input clock x 12 (Connectivity line devices)
#define RCC_PLL23_MUX13         ((u8) 0x0B) //PLL23 input clock x 13 (Connectivity line devices)
#define RCC_PLL23_MUX14         ((u8) 0x0C) //PLL23 input clock x 14 (Connectivity line devices)
#define RCC_PLL23_MUX16         ((u8) 0x0E) //PLL23 input clock x 16 (Connectivity line devices)
#define RCC_PLL23_MUX20         ((u8) 0x0F) //PLL23 input clock x 20 (Connectivity line devices)
#define RCC_PLL_HSE             ((u8) 0x00) //HSE oscillator clock selected as PREDIV1 clock entry (Connectivity line devices)
#define RCC_PLL_PLL2            ((u8) 0x01) //PLL2 clock selected as PREDIV1 clock entry (Connectivity line devices)
#define RCC_I2S_SYS             ((u8) 0x00) //System clock (SYSCLK) selected as I2S clock entry (Connectivity line devices)
#define RCC_I2S_PLL3            ((u8) 0x01) //PLL3 VCO clock selected as I2S clock entry (Connectivity line devices)
/****************************************** GPIO **************************************************/
typedef struct
{
  struct
  {
    unsigned MODE00     : 2; //Port x mode bits 0
    unsigned CNF00      : 2; //Port x configuration bits 0
    unsigned MODE01     : 2; //Port x mode bits 1
    unsigned CNF01      : 2; //Port x configuration bits 1
    unsigned MODE02     : 2; //Port x mode bits 2
    unsigned CNF02      : 2; //Port x configuration bits 2
    unsigned MODE03     : 2; //Port x mode bits 3
    unsigned CNF03      : 2; //Port x configuration bits 3
    unsigned MODE04     : 2; //Port x mode bits 4
    unsigned CNF04      : 2; //Port x configuration bits 4
    unsigned MODE05     : 2; //Port x mode bits 5
    unsigned CNF05      : 2; //Port x configuration bits 5
    unsigned MODE06     : 2; //Port x mode bits 6
    unsigned CNF06      : 2; //Port x configuration bits 6
    unsigned MODE07     : 2; //Port x mode bits 7
    unsigned CNF07      : 2; //Port x configuration bits 7
  }volatile CRL;             //Port configuration register low (GPIO_CRL) (x=A..G)
  struct
  {
    unsigned MODE08     : 2; //Port x mode bits 8
    unsigned CNF08      : 2; //Port x configuration bits 8
    unsigned MODE09     : 2; //Port x mode bits 9
    unsigned CNF09      : 2; //Port x configuration bits 9
    unsigned MODE10     : 2; //Port x mode bits 10
    unsigned CNF10      : 2; //Port x configuration bits 10
    unsigned MODE11     : 2; //Port x mode bits 11
    unsigned CNF11      : 2; //Port x configuration bits 11
    unsigned MODE12     : 2; //Port x mode bits 12
    unsigned CNF12      : 2; //Port x configuration bits 12
    unsigned MODE13     : 2; //Port x mode bits 13
    unsigned CNF13      : 2; //Port x configuration bits 13
    unsigned MODE14     : 2; //Port x mode bits 14
    unsigned CNF14      : 2; //Port x configuration bits 14
    unsigned MODE15     : 2; //Port x mode bits 15
    unsigned CNF15      : 2; //Port x configuration bits 15
  }volatile CRH;             //Port configuration register high (GPIO_CRH) (x=A..G)
  struct
  {
    unsigned IR         : 16; //Port input data register
    unsigned RSVD0      : 16; //Reserved
  }volatile IDR;              //Port input data register (GPIO_IDR) (x=A..G)
  struct
  {
    unsigned OR         : 16; //Port output data register
    unsigned RSVD0      : 16; //Reserved
  }volatile ODR;              //Port output data register (GPIO_ODR) (x=A..G)
  struct
  {
    unsigned BS         : 16; //Port x Set bit y (y= 0 .. 15)
    unsigned BR         : 16; //Port x Reset bit y (y= 0 .. 15)
  }volatile BSRR;             //Port bit set/reset register (GPIO_BSRR) (x=A..G)
  struct
  {
    unsigned BR         : 16; //Port bit reset register
    unsigned RSVD0      : 16; //Reserved
  }volatile BRR;              //Port bit reset register (GPIO_BRR) (x=A..G)
  struct
  {
    unsigned LCK        : 16; //Port x Lock bit y (y= 0 .. 15)
    unsigned LCKK       : 1;  //Lock key
    unsigned RSVD0      : 15; //Reserved
  }volatile LCKR;             //Port configuration lock register (GPIO_LCKR) (x=A..G)
}GPIO_Type;
#define GPIOA ((GPIO_Type*) GPIOA_BASE)
#define GPIOB ((GPIO_Type*) GPIOB_BASE)
#define GPIOC ((GPIO_Type*) GPIOC_BASE)
#define GPIOD ((GPIO_Type*) GPIOD_BASE)
#define GPIOE ((GPIO_Type*) GPIOE_BASE)
#define GPIOG ((GPIO_Type*) GPIOG_BASE)
#define GPIOF ((GPIO_Type*) GPIOF_BASE)
#define GPIO_CFG_PIN00(PORT, MODE) PORT->CRL.CNF00 = (MODE & 3);PORT->CRL.MODE00 = ((MODE >> 2) & 3); PORT->BSRR.BS = (((MODE >> 4) & 1) << 0x0); PORT->BSRR.BR = (((MODE >> 5) & 1) << 0x0) //Port bit 00 configuration
#define GPIO_CFG_PIN01(PORT, MODE) PORT->CRL.CNF01 = (MODE & 3); PORT->CRL.MODE01 = ((MODE >> 2) & 3); PORT->BSRR.BS = (((MODE >> 4) & 1) << 0x1); PORT->BSRR.BR = (((MODE >> 5) & 1) << 0x1) //Port bit 01 configuration
#define GPIO_CFG_PIN02(PORT, MODE) PORT->CRL.CNF02 = (MODE & 3); PORT->CRL.MODE02 = ((MODE >> 2) & 3); PORT->BSRR.BS = (((MODE >> 4) & 1) << 0x2); PORT->BSRR.BR = (((MODE >> 5) & 1) << 0x2) //Port bit 02 configuration
#define GPIO_CFG_PIN03(PORT, MODE) PORT->CRL.CNF03 = (MODE & 3); PORT->CRL.MODE03 = ((MODE >> 2) & 3); PORT->BSRR.BS = (((MODE >> 4) & 1) << 0x3); PORT->BSRR.BR = (((MODE >> 5) & 1) << 0x3) //Port bit 03 configuration
#define GPIO_CFG_PIN04(PORT, MODE) PORT->CRL.CNF04 = (MODE & 3); PORT->CRL.MODE04 = ((MODE >> 2) & 3); PORT->BSRR.BS = (((MODE >> 4) & 1) << 0x4); PORT->BSRR.BR = (((MODE >> 5) & 1) << 0x4) //Port bit 04 configuration
#define GPIO_CFG_PIN05(PORT, MODE) PORT->CRL.CNF05 = (MODE & 3); PORT->CRL.MODE05 = ((MODE >> 2) & 3); PORT->BSRR.BS = (((MODE >> 4) & 1) << 0x5); PORT->BSRR.BR = (((MODE >> 5) & 1) << 0x5) //Port bit 05 configuration
#define GPIO_CFG_PIN06(PORT, MODE) PORT->CRL.CNF06 = (MODE & 3); PORT->CRL.MODE06 = ((MODE >> 2) & 3); PORT->BSRR.BS = (((MODE >> 4) & 1) << 0x6); PORT->BSRR.BR = (((MODE >> 5) & 1) << 0x6) //Port bit 06 configuration
#define GPIO_CFG_PIN07(PORT, MODE) PORT->CRL.CNF07 = (MODE & 3); PORT->CRL.MODE07 = ((MODE >> 2) & 3); PORT->BSRR.BS = (((MODE >> 4) & 1) << 0x7); PORT->BSRR.BR = (((MODE >> 5) & 1) << 0x7) //Port bit 07 configuration
#define GPIO_CFG_PIN08(PORT, MODE) PORT->CRH.CNF08 = (MODE & 3); PORT->CRH.MODE08 = ((MODE >> 2) & 3); PORT->BSRR.BS = (((MODE >> 4) & 1) << 0x8); PORT->BSRR.BR = (((MODE >> 5) & 1) << 0x8) //Port bit 08 configuration
#define GPIO_CFG_PIN09(PORT, MODE) PORT->CRH.CNF09 = (MODE & 3); PORT->CRH.MODE09 = ((MODE >> 2) & 3); PORT->BSRR.BS = (((MODE >> 4) & 1) << 0x9); PORT->BSRR.BR = (((MODE >> 5) & 1) << 0x9) //Port bit 09 configuration
#define GPIO_CFG_PIN10(PORT, MODE) PORT->CRH.CNF10 = (MODE & 3); PORT->CRH.MODE10 = ((MODE >> 2) & 3); PORT->BSRR.BS = (((MODE >> 4) & 1) << 0xA); PORT->BSRR.BR = (((MODE >> 5) & 1) << 0xA) //Port bit 10 configuration
#define GPIO_CFG_PIN11(PORT, MODE) PORT->CRH.CNF11 = (MODE & 3); PORT->CRH.MODE11 = ((MODE >> 2) & 3); PORT->BSRR.BS = (((MODE >> 4) & 1) << 0xB); PORT->BSRR.BR = (((MODE >> 5) & 1) << 0xB) //Port bit 11 configuration
#define GPIO_CFG_PIN12(PORT, MODE) PORT->CRH.CNF12 = (MODE & 3); PORT->CRH.MODE12 = ((MODE >> 2) & 3); PORT->BSRR.BS = (((MODE >> 4) & 1) << 0xC); PORT->BSRR.BR = (((MODE >> 5) & 1) << 0xC) //Port bit 12 configuration
#define GPIO_CFG_PIN13(PORT, MODE) PORT->CRH.CNF13 = (MODE & 3); PORT->CRH.MODE13 = ((MODE >> 2) & 3); PORT->BSRR.BS = (((MODE >> 4) & 1) << 0xD); PORT->BSRR.BR = (((MODE >> 5) & 1) << 0xD) //Port bit 13 configuration
#define GPIO_CFG_PIN14(PORT, MODE) PORT->CRH.CNF14 = (MODE & 3); PORT->CRH.MODE14 = ((MODE >> 2) & 3); PORT->BSRR.BS = (((MODE >> 4) & 1) << 0xE); PORT->BSRR.BR = (((MODE >> 5) & 1) << 0xE) //Port bit 14 configuration
#define GPIO_CFG_PIN15(PORT, MODE) PORT->CRH.CNF15 = (MODE & 3); PORT->CRH.MODE15 = ((MODE >> 2) & 3); PORT->BSRR.BS = (((MODE >> 4) & 1) << 0xF); PORT->BSRR.BR = (((MODE >> 5) & 1) << 0xF) //Port bit 15 configuration
#define GPIO_IN(PORT)           PORT->IDR.IR                    //Port input data register
#define GPIO_OUT(PORT)          PORT->ODR.OR                    //Port output data register
#define GPIO_BIT_SET(PORT)      PORT->BSRR.BS                   //Port Set bit
#define GPIO_BIT_RST(PORT)      PORT->BSRR.BR                   //Port Reset bit
#define GPIO_LCK(PORT)          PORT->LCKR.LCK                  //Port Lock bit
#define GPIO_LCK_KEY(PORT)      PORT->LCKR.LCKK                 //Port Lock key
#define GPIO_ANALOG             ((u8) 0x00)                     //Input/output Analog
#define GPIO_IN_FLOAT           ((u8) 0x01)                     //Input Floating
#define GPIO_IN_PU              ((u8) 0x12)                     //Input PU
#define GPIO_IN_PD              ((u8) 0x22)                     //Input PD
#define GPIO_OUT_PP_PU_L        ((u8) 0x18)                     //GP output PP + PU, max speed 2 MHz
#define GPIO_OUT_PP_PU_M        ((u8) 0x14)                     //GP output PP + PU, max speed 10 MHz
#define GPIO_OUT_PP_PU_H        ((u8) 0x1C)                     //GP output PP + PU, max speed 50 MHz
#define GPIO_OUT_PP_PD_L        ((u8) 0x28)                     //GP output PP + PD, max speed 2 MHz
#define GPIO_OUT_PP_PD_M        ((u8) 0x24)                     //GP output PP + PD, max speed 10 MHz
#define GPIO_OUT_PP_PD_H        ((u8) 0x2C)                     //GP output PP + PD, max speed 50 MHz
#define GPIO_OUT_OD_PU_L        ((u8) 0x19)                     //GP output OD + PU, max speed 2 MHz
#define GPIO_OUT_OD_PU_M        ((u8) 0x15)                     //GP output OD + PU, max speed 10 MHz
#define GPIO_OUT_OD_PU_H        ((u8) 0x1D)                     //GP output OD + PU, max speed 50 MHz
#define GPIO_OUT_OD_PD_L        ((u8) 0x29)                     //GP output OD + PD, max speed 2 MHz
#define GPIO_OUT_OD_PD_M        ((u8) 0x25)                     //GP output OD + PD, max speed 10 MHz
#define GPIO_OUT_OD_PD_H        ((u8) 0x2D)                     //GP output OD + PD, max speed 50 MHz
#define GPIO_AF_PP_PU_L         ((u8) 0x1A)                     //AF PP + PU, max speed 2 MHz
#define GPIO_AF_PP_PU_M         ((u8) 0x16)                     //AF PP + PU, max speed 10 MHz
#define GPIO_AF_PP_PU_H         ((u8) 0x1E)                     //AF PP + PU, max speed 50 MHz
#define GPIO_AF_PP_PD_L         ((u8) 0x2A)                     //AF PP + PD, max speed 2 MHz
#define GPIO_AF_PP_PD_M         ((u8) 0x26)                     //AF PP + PD, max speed 10 MHz
#define GPIO_AF_PP_PD_H         ((u8) 0x2E)                     //AF PP + PD, max speed 50 MHz
#define GPIO_AF_OD_PU_L         ((u8) 0x1B)                     //AF OD + PU, max speed 2 MHz
#define GPIO_AF_OD_PU_M         ((u8) 0x17)                     //AF OD + PU, max speed 10 MHz
#define GPIO_AF_OD_PU_H         ((u8) 0x1F)                     //AF OD + PU, max speed 50 MHz
#define GPIO_AF_OD_PD_L         ((u8) 0x2B)                     //AF OD + PD, max speed 2 MHz
#define GPIO_AF_OD_PD_M         ((u8) 0x27)                     //AF OD + PD, max speed 10 MHz
#define GPIO_AF_OD_PD_H         ((u8) 0x2F)                     //AF OD + PD, max speed 50 MHz
#define GPIO_PIN00              ((u16) 0x0001)
#define GPIO_PIN01              ((u16) 0x0002)
#define GPIO_PIN02              ((u16) 0x0004)
#define GPIO_PIN03              ((u16) 0x0008)
#define GPIO_PIN04              ((u16) 0x0010)
#define GPIO_PIN05              ((u16) 0x0020)
#define GPIO_PIN06              ((u16) 0x0040)
#define GPIO_PIN07              ((u16) 0x0080)
#define GPIO_PIN08              ((u16) 0x0100)
#define GPIO_PIN09              ((u16) 0x0200)
#define GPIO_PIN10              ((u16) 0x0400)
#define GPIO_PIN11              ((u16) 0x0800)
#define GPIO_PIN12              ((u16) 0x1000)
#define GPIO_PIN13              ((u16) 0x2000)
#define GPIO_PIN14              ((u16) 0x4000)
#define GPIO_PIN15              ((u16) 0x8000)
/******************************************* ADC **************************************************/
typedef struct
{
  struct
  {
    unsigned AWD        : 1;    //Analog watchdog flag
    unsigned EOC        : 1;    //End of conversion
    unsigned JEOC       : 1;    //Injected channel end of conversion
    unsigned JSTRT      : 1;    //Injected channel Start flag
    unsigned STRT       : 1;    //Regular channel Start flag
    unsigned RSVD0      : 27;   //Reserved
  }
  volatile SR;                  //ADC status register (ADC_SR)
  
  struct
  {
    unsigned AWDCH      : 5;    //Analog watchdog channel select bits
    unsigned EOCIE      : 1;    //Interrupt enable for EOC
    unsigned AWDIE      : 1;    //Analog watchdog interrupt enable
    unsigned JEOCIE     : 1;    //Interrupt enable for injected channels
    unsigned SCAN       : 1;    //Scan mode
    unsigned AWDSGL     : 1;    //Enable the watchdog on a single channel in scan mode
    unsigned JAUTO      : 1;    //Automatic Injected Group conversion
    unsigned DISCEN     : 1;    //Discontinuous mode on regular channels
    unsigned JDISCEN    : 1;    //Discontinuous mode on injected channels
    unsigned DISCNUM    : 3;    //Discontinuous mode channel count
    unsigned DUALMOD    : 4;    //Dual mode selection
    unsigned RSVD0      : 2;    //Reserved
    unsigned JAWDEN     : 1;    //Analog watchdog enable on injected channels
    unsigned AWDEN      : 1;    //Analog watchdog enable on regular channels
    unsigned RSVD1      : 8;    //Reserved
  }
  volatile CR1;              //ADC control register 1
 
  struct
  {
    unsigned ADON       : 1;    //A/D converter ON / OFF
    unsigned CONT       : 1;    //Continuous conversion
    unsigned CAL        : 1;    //A/D Calibration
    unsigned RSTCAL     : 1;    //Reset calibration
    unsigned RSVD0      : 4;    //Reserved
    unsigned DMA        : 1;    //Direct memory access mode
    unsigned RSVD1      : 2;    //Reserved
    unsigned ALIGN      : 1;    //Data alignment
    unsigned JEXTSEL    : 3;    //External event select for injected group
    unsigned JEXTTRIG   : 1;    //External trigger conversion mode for injected channels
    unsigned RSVD2      : 1;    //Reserved
    unsigned EXTSEL     : 3;    //External event select for regular group
    unsigned EXTTRIG    : 1;    //External trigger conversion mode for regular channels
    unsigned JSWSTART   : 1;    //Start conversion of injected channels
    unsigned SWSTART    : 1;    //Start conversion of regular channels
    unsigned TSVREFE    : 1;    //Temperature sensor and VREFINT enable 
    unsigned RSVD3      : 8;    //Reserved
  }
  volatile CR2;              //ADC control register 2
  
  struct
  {
    unsigned SMP10      : 3;    //Channel 10 Sample time selection
    unsigned SMP11      : 3;    //Channel 11 Sample time selection
    unsigned SMP12      : 3;    //Channel 12 Sample time selection
    unsigned SMP13      : 3;    //Channel 13 Sample time selection
    unsigned SMP14      : 3;    //Channel 14 Sample time selection
    unsigned SMP15      : 3;    //Channel 15 Sample time selection
    unsigned SMP16      : 3;    //Channel 16 Sample time selection
    unsigned SMP17      : 3;    //Channel 17 Sample time selection
    unsigned RSVD0      : 8;    //Reserved
  }
  volatile SMPR1;           //ADC sample time register 1
  
   struct
  {
    unsigned SMP0       : 3;    //Channel 0 Sample time selection
    unsigned SMP1       : 3;    //Channel 1 Sample time selection
    unsigned SMP2       : 3;    //Channel 2 Sample time selection
    unsigned SMP3       : 3;    //Channel 3 Sample time selection
    unsigned SMP4       : 3;    //Channel 4 Sample time selection
    unsigned SMP5       : 3;    //Channel 5 Sample time selection
    unsigned SMP6       : 3;    //Channel 6 Sample time selection
    unsigned SMP7       : 3;    //Channel 7 Sample time selection
    unsigned SMP8       : 3;    //Channel 8 Sample time selection
    unsigned SMP9       : 3;    //Channel 9 Sample time selection
    unsigned RSVD0      : 2;    //Reserved
  }
  volatile SMPR2;          //ADC sample time register 2
  
  struct
  {
    unsigned JOFFSET1   : 12;   //Data offset for injected channel 1
    unsigned RSVD0      : 20;   //Reserved
  }
  volatile JOFR1;          //ADC injected channel data offset register 1
  
   struct
  {
    unsigned JOFFSET2   : 12;   //Data offset for injected channel 2
    unsigned RSVD0      : 20;   //Reserved
  }
  volatile JOFR2;          //ADC injected channel data offset register 2
  
   struct
  {
    unsigned JOFFSET3   : 12;   //Data offset for injected channel 3
    unsigned RSVD0      : 20;   //Reserved
  }
  volatile JOFR3;          //ADC injected channel data offset register 3
  
   struct
  {
    unsigned JOFFSET4   : 12;   //Data offset for injected channel 4
    unsigned RSVD0      : 20;   //Reserved
  }
  volatile JOFR4;          //ADC injected channel data offset register 4
  
 struct
  {
    unsigned HT         : 12;   //Analog watchdog high threshold
    unsigned RSVD0      : 20;   //Reserved
  }
  volatile HTR;            //ADC watchdog high threshold register
  
 struct
  {
    unsigned LT         : 12;   //Analog watchdog low threshold
    unsigned RSVD0      : 20;   //Reserved
  }
  volatile LTR;            //ADC watchdog low threshold register
 
 struct
  {
    unsigned SQ13       : 5;    //13th conversion in regular sequence
    unsigned SQ14       : 5;    //14th conversion in regular sequence
    unsigned SQ15       : 5;    //15th conversion in regular sequence
    unsigned SQ16       : 5;    //16th conversion in regular sequence
    unsigned L          : 4;    //Regular channel sequence length
    unsigned RSVD0      : 8;    //Reserved
  }
  volatile SQR1;           //ADC regular sequence register 1
 
  struct
  {
    unsigned SQ7        : 5;    //7th conversion in regular sequence
    unsigned SQ8        : 5;    //8th conversion in regular sequence
    unsigned SQ9        : 5;    //9th conversion in regular sequence
    unsigned SQ10       : 5;    //10th conversion in regular sequence
    unsigned SQ11       : 5;    //11th conversion in regular sequence
    unsigned SQ12       : 5;    //12th conversion in regular sequence
    unsigned RSVD0      : 2;    //Reserved
  }
  volatile SQR2;           //ADC regular sequence register 2
 
  struct
  {
    unsigned SQ1        : 5;    //1th conversion in regular sequence
    unsigned SQ2        : 5;    //2th conversion in regular sequence
    unsigned SQ3        : 5;    //3th conversion in regular sequence
    unsigned SQ4        : 5;    //4th conversion in regular sequence
    unsigned SQ5        : 5;    //5th conversion in regular sequence
    unsigned SQ6        : 5;    //6th conversion in regular sequence
    unsigned RSVD0      : 2;    //Reserved
  }
  volatile SQR3;           //ADC regular sequence register 3
  
  struct
  {
    unsigned JSQ1       : 5;    //first conversion in injected sequence (when JL[1:0] = 3)
    unsigned JSQ2       : 5;    //second conversion in injected sequence (when JL[1:0] = 3)
    unsigned JSQ3       : 5;    //third conversion in injected sequence (when JL[1:0] = 3)
    unsigned JSQ4       : 5;    //fourth conversion in injected sequence (when JL[1:0] = 3)
    unsigned JL         : 2;    //Injected sequence length
    unsigned RSVD0      : 10;   //Reserved
  }
  volatile JSQR;           //ADC injected sequence register (ADC_JSQR)
  
  struct
  {
    unsigned JDATA1      : 16;   //Injected data
    unsigned RSVD0      : 16;   //Reserved
  }
  volatile JDR1;            //ADC injected data register 1 
  
   struct
  {
    unsigned JDATA2      : 16;   //Injected data
    unsigned RSVD0      : 16;   //Reserved
  }
  volatile JDR2;            //ADC injected data register 2 
  
   struct
  {
    unsigned JDATA3      : 16;   //Injected data
    unsigned RSVD0      : 16;   //Reserved
  }
  volatile JDR3;            //ADC injected data register 3 
  
   struct
  {
    unsigned JDATA4      : 16;   //Injected data
    unsigned RSVD0      : 16;   //Reserved
  }
  volatile JDR4;            //ADC injected data register 4 
  
    struct
  {
    unsigned DATA       : 16;    //Regular data
    unsigned ADC2DATA   : 16;    //ADC2 data
  }
  volatile DR;               //ADC regular data register 
}ADC_Type;

// FOR ADC1
#define ADC ((ADC_Type*) ADC1_BASE)

//ADC status register
#define ADC_AWD         ADC->SR.AWD                //Analog watchdog flag
#define ADC_EOC         ADC->SR.EOC                //End of conversion
#define ADC_JEOC        ADC->SR.JEOC               //Injected channel end of conversio
#define ADC_JSTRT       ADC->SR.JSTRT              //Injected channel Start fla
#define ADC_STRT        ADC->SR.STRT               //Regular channel Start flag

//ADC control register 1
#define ADC_AWDCH       ADC->CR1.AWDCH             //Analog watchdog channel select bits
#define ADC_EOCIE       ADC->CR1.EOCIE             //Interrupt enable for EOC
#define ADC_AWDIE       ADC->CR1.AWDIE             //Analog watchdog interrupt enable
#define ADC_JEOCIE      ADC->CR1.JEOCIE            //Interrupt enable for injected channels
#define ADC_SCAN        ADC->CR1.SCAN              //Scan mode
#define ADC_AWDSGL      ADC->CR1.AWDSGL            //Enable the watchdog on a single channel in scan mode
#define ADC_JAUTO       ADC->CR1.JAUTO             //Automatic Injected Group conversion
#define ADC_DISCEN      ADC->CR1.DISCEN            //Discontinuous mode on regular channels
#define ADC_JDISCEN     ADC->CR1.JDISCEN           //Discontinuous mode on injected channels
#define ADC_DISCNUM     ADC->CR1.DISCNUM           //Discontinuous mode channel count
#define ADC_DUALMOD     ADC->CR1.DUALMOD           //Dual mode selection
#define ADC_JAWDEN      ADC->CR1.JAWDEN            //Analog watchdog enable on injected channels
#define ADC_AWDEN       ADC->CR1.AWDEN             //Analog watchdog enable on regular channels

//ADC control register 2
#define ADC_ADON       ADC->CR2.ADON               //A/D converter ON / OFF
#define ADC_CONT       ADC->CR2.CONT               //Continuous conversion
#define ADC_CAL        ADC->CR2.CAL                //A/D Calibration ??
#define ADC_RSTCAL     ADC->CR2.RSTCAL             //Reset calibration ??
#define ADC_DMA        ADC->CR2.DMA                //Direct memory access mode
#define ADC_ALIGN      ADC->CR2.ALIGN              //Data alignment
#define ADC_JEXTSEL    ADC->CR2.JEXTSEL            //External event select for injected group
#define ADC_JEXTTRIG   ADC->CR2.JEXTTRIG           //External trigger conversion mode for injected channels
#define ADC_EXTSEL     ADC->CR2.EXTSEL             //External event select for regular group
#define ADC_EXTTRIG    ADC->CR2.EXTTRIG            //External trigger conversion mode for regular channels
#define ADC_JSWSTART   ADC->CR2.JSWSTART           //Start conversion of injected channels ??
#define ADC_SWSTART    ADC->CR2.SWSTART            //Start conversion of regular channels ??
#define ADC_TSVREFE    ADC->CR2.TSVREFE            //Temperature sensor and VREFINT enable 

//ADC sample time register 1
#define ADC_SMP10      ADC->SMPR1.SMP10    //Channel 10 Sample time selection
#define ADC_SMP11      ADC->SMPR1.SMP11    //Channel 11 Sample time selection
#define ADC_SMP12      ADC->SMPR1.SMP12    //Channel 12 Sample time selection
#define ADC_SMP13      ADC->SMPR1.SMP13    //Channel 13 Sample time selection
#define ADC_SMP14      ADC->SMPR1.SMP14    //Channel 14 Sample time selection
#define ADC_SMP15      ADC->SMPR1.SMP15    //Channel 15 Sample time selection
#define ADC_SMP16      ADC->SMPR1.SMP16    //Channel 16 Sample time selection
#define ADC_SMP17      ADC->SMPR1.SMP17    //Channel 17 Sample time selection

//ADC sample time register 2
#define ADC_SMP0      ADC->SMPR2.SMP0    //Channel 0 Sample time selection
#define ADC_SMP1      ADC->SMPR2.SMP1    //Channel 1 Sample time selection
#define ADC_SMP2      ADC->SMPR2.SMP2    //Channel 2 Sample time selection
#define ADC_SMP3      ADC->SMPR2.SMP3    //Channel 3 Sample time selection
#define ADC_SMP4      ADC->SMPR2.SMP4    //Channel 4 Sample time selection
#define ADC_SMP5      ADC->SMPR2.SMP5    //Channel 5 Sample time selection
#define ADC_SMP6      ADC->SMPR2.SMP6    //Channel 6 Sample time selection
#define ADC_SMP7      ADC->SMPR2.SMP7    //Channel 7 Sample time selection
#define ADC_SMP8      ADC->SMPR2.SMP8    //Channel 8 Sample time selection
#define ADC_SMP9      ADC->SMPR2.SMP9    //Channel 9 Sample time selection

//ADC injected channel data offset register 1
#define ADC_JOFFSET1  ADC->JOFR1.JOFFSET1   //Data offset for injected channel 1

//ADC injected channel data offset register 2
#define ADC_JOFFSET2  ADC->JOFR2.JOFFSET2   //Data offset for injected channel 2

//ADC injected channel data offset register 3
#define ADC_JOFFSET3  ADC->JOFR3.JOFFSET3   //Data offset for injected channel 3

//ADC injected channel data offset register 4
#define ADC_JOFFSET4  ADC->JOFR4.JOFFSET4   //Data offset for injected channel 4

//ADC watchdog high threshold register
#define ADC_HT        ADC->HTR.HT               //Analog watchdog high threshold

//ADC watchdog low threshold register
#define ADC_LT        ADC->HTR.LT               //Analog watchdog low threshold

//ADC regular sequence register 1
#define ADC_SQ13      ADC->SQR1.SQ13    //13th conversion in regular sequence
#define ADC_SQ14      ADC->SQR1.SQ14    //14th conversion in regular sequence
#define ADC_SQ15      ADC->SQR1.SQ15    //15th conversion in regular sequence
#define ADC_SQ16      ADC->SQR1.SQ16    //16th conversion in regular sequence
#define ADC_L         ADC->SQR1.L       //Regular channel sequence length

//ADC regular sequence register 2
#define ADC_SQ12      ADC->SQR2.SQ12    //12th conversion in regular sequence
#define ADC_SQ11      ADC->SQR2.SQ11    //11th conversion in regular sequence
#define ADC_SQ10      ADC->SQR2.SQ10    //10th conversion in regular sequence
#define ADC_SQ9       ACC->SQR2.SQ9     //9th conversion in regular sequence
#define ADC_SQ8       ADC->SQR2.SQ8     //8th conversion in regular sequence
#define ADC_SQ7       ADC->SQR2.SQ7     //7th conversion in regular sequence

//ADC regular sequence register 3
#define ADC_SQ6       ADC->SQR3.SQ6      //6th conversion in regular sequence
#define ADC_SQ5       ADC->SQR3.SQ5      //5th conversion in regular sequence
#define ADC_SQ4       ADC->SQR3.SQ4      //4th conversion in regular sequence
#define ADC_SQ3       ACC->SQR3.SQ3      //3th conversion in regular sequence
#define ADC_SQ2       ADC->SQR3.SQ2      //2th conversion in regular sequence
#define ADC_SQ1       ADC->SQR3.SQ1      //1th conversion in regular sequence

//ADC injected sequence register
#define ADC_JSQ1      ADC->JSQR.JSQ1     //first conversion in injected sequence (when JL[1:0] = 3)
#define ADC_JSQ2      ADC->JSQR.JSQ2     //second conversion in injected sequence (when JL[1:0] = 3)
#define ADC_JSQ3      ADC->JSQR.JSQ3     //third conversion in injected sequence (when JL[1:0] = 3)
#define ADC_JSQ4      ACC->JSQR.JSQ4     //fourth conversion in injected sequence (when JL[1:0] = 3)
#define ADC_JL        ADC->JSQR.JL       //Injected sequence length
         
//ADC injected data register 1 
#define ADC_JDATA1     ADC->JDR1.JDATA1    //Injected data

//ADC injected data register 2 
#define ADC_JDATA2     ADC->JDR2.JDATA2    //Injected data

//ADC injected data register 3 
#define ADC_JDATA3     ADC->JDR3.JDATA3    //Injected data

//ADC injected data register 4 
#define ADC_JDATA4     ADC->JDR4.JDATA4    //Injected data

//ADC regular data register 
#define ADC_DATA       ADC->DR.DATA        //Regular data
#define ADC_ADC2DATA   ADC->DR.ADC2DATA    //ADC2 data

/******************************************* I2C**************************************************/
typedef struct
{
  struct
  {
    unsigned PE         : 1; //Peripheral enable
    unsigned SMBUS      : 1; //SMBus type
    unsigned RSVD0      : 1; //Reserved
    unsigned SMBTYPE    : 1; //SMBus type
    unsigned ENARP      : 1; //ARP enable
    unsigned ENPEC      : 1; //PEC enable
    unsigned ENGC       : 1; //General call enable
    unsigned NOSTRETCH  : 1; //Clock stretching disable (Slave mode)
    unsigned START      : 1; //Start generation
    unsigned STOP       : 1; //Stop generation
    unsigned ACK        : 1; //Acknowledge enable
    unsigned POS        : 1; //Acknowledge/PEC Position (for data reception)
    unsigned PEC        : 1; //Packet error checking
    unsigned ALERT      : 1; //SMBus alert
    unsigned RSVD1      : 1; //Reserved
    unsigned SWRST      : 1; //Software reset  
  }volatile I2C_CR1;         //I2C Control register 1
  struct
  {   
    unsigned FREQ       : 6; //Peripheral clock frequency
    unsigned RSVD0      : 2; //Reserved 
    unsigned ITERREN    : 1; //Error interrupt enable
    unsigned ITEVTEN    : 1; //Event interrupt enable
    unsigned ITBUFEN    : 1; //Buffer interrupt enable
    unsigned DMAEN      : 1; //DMA requests enable
    unsigned LAST       : 1; //DMA last transfer
    unsigned RSVD1      : 3; //Reserved
  }volatile I2C_CR2;         //I2C Control register 2  
  struct
  {
    unsigned ADD0       : 1; //Interface address
    unsigned ADD      : 7; //Interface address
    unsigned ADD8    : 2; //Interface address???
    unsigned RSVD0      : 4; //Reserved
    unsigned ABK    : 1; //Should always be kept at 1 by software
    unsigned ADDMODE    : 1; //Addressing mode (slave mode)
  }volatile I2C_OAR1;         //I2C Own address register 1  
  struct
  {  
    unsigned ENDUAL      : 1; //Dual addressing mode enable
    unsigned ADD2    : 7; //Interface address
    unsigned RSVD0      : 8; //Reserved     
  }volatile I2C_OAR2;         //I2C Own address register 2
  struct
  {
    unsigned DR      : 8; //8-bit data register
    unsigned RSVD0      : 8; //Reserved    
  }volatile I2C_DR;         //I2C Data register
  struct
  { 
    unsigned SB      : 1; //Start bit (Master mode)
    unsigned ADDR      : 1; //Address sent (master mode)/matched (slave mode)
    unsigned BTF      : 1; //Byte transfer finished
    unsigned ADD10      : 1; //10-bit header sent (Master mode)
    unsigned STOPF      : 1; //Stop detection (slave mode)
    unsigned RSVD0      : 1; //Reserved 
    unsigned RxNE      : 1; //Data register not empty (receivers)
    unsigned TxE      : 1; //Data register empty (transmitters)
    unsigned BERR      : 1; //Bus error
    unsigned ARLO      : 1; //Arbitration lost (master mode)
    unsigned AF      : 1; //Acknowledge failure 
    unsigned OVR      : 1; //Overrun/Underrun
    unsigned PECERR      : 1; //PEC Error in reception
    unsigned RSVD1      : 1; //Reserved 
    unsigned TIMEOUT      : 1; //Timeout or Tlow error
    unsigned SMBALERT      : 1; //SMBus alert    
  }volatile I2C_SR1;         //I2C Status register 1 
  struct
  {  
    unsigned MSL      : 1; //Master/slave
    unsigned BUSY      : 1; //Bus busy
    unsigned TRA     : 1; //Transmitter/receiver
    unsigned RSVD0      : 1; //Reserved
    unsigned GENCALL      : 1; //General call address (Slave mode)
    unsigned SMBDEFAULT      : 1; //SMBus device default address (Slave mode)
    unsigned SMBHOST      : 1; //SMBus host header (Slave mode)
    unsigned DUALF      : 1; //Dual flag (Slave mode)
    unsigned PEC      : 8; //Packet error checking register 
  }volatile I2C_SR2;         //I2C Status register 2
  struct
  {  
    unsigned CCR      : 12; //Clock control register in Fm/Sm mode (Master mode)
    unsigned RSVD0      : 2; //Reserved
    unsigned DUTY      : 1; //Fm mode duty cycle
    unsigned FS      : 1; //I2C master mode selection  
  }volatile I2C_CCR;         //I2C Clock control register  
  struct
  { 
    unsigned TRISE      : 6; //Maximum rise time in Fm/Sm mode (Master mode)
    unsigned RSVD0      : 10; //Reserved  
  }volatile I2C_TRISE;         //I2C TRISE register
}I2C_Type;

#define I2C ((I2C_Type*) I2C1_BASE)
//I2C Control register 1
#define I2C_PE          I2C->I2C_CR1.PE         //Peripheral enable
#define I2C_SMBUS       I2C->I2C_CR1.SMBUS      //SMBus type
#define I2C_SMBTYPE     I2C->I2C_CR1.SMBTYPE    //SMBus type
#define I2C_ENARP       I2C->I2C_CR1.ENARP      //ARP enable
#define I2C_ENPEC       I2C->I2C_CR1.ENPEC      //PEC enable
#define I2C_ENGC        I2C->I2C_CR1.ENGC       //General call enable
#define I2C_NOSTRETCH   I2C->I2C_CR1.NOSTRETCH  //Clock stretching disable (Slave mode)
#define I2C_START       I2C->I2C_CR1.START      //Start generation
#define I2C_STOP        I2C->I2C_CR1.STOP       //Stop generation
#define I2C_ACK         I2C->I2C_CR1.ACK        //Acknowledge enable
#define I2C_POS         I2C->I2C_CR1.POS        //Acknowledge/PEC Position (for data reception)
#define I2C_PEC_SR2     I2C->I2C_CR1.PEC        //Packet error checking
#define I2C_ALERT       I2C->I2C_CR1.ALERT      //SMBus alert
#define I2C_SWRST       I2C->I2C_CR1.SWRST      //Counter enable
//I2C Control register 2 
#define I2C_FREQ        I2C->I2C_CR2.FREQ       //Peripheral clock frequency
#define I2C_ITERREN     I2C->I2C_CR2.ITERREN    //Error interrupt enable 
#define I2C_ITEVTEN     I2C->I2C_CR2.ITEVTEN    //Event interrupt enable
#define I2C_ITBUFEN     I2C->I2C_CR2.ITBUFEN    //Buffer interrupt enable
#define I2C_DMAEN       I2C->I2C_CR2.DMAEN      //DMA requests enable
#define I2C_LAST        I2C->I2C_CR2.LAST       //DMA last transfer
//I2C Own address register 1
#define I2C_ADD0        I2C->I2C_OAR1.ADD0      //Interface address
#define I2C_ADD         I2C->I2C_OAR1.ADD       //Interface address
#define I2C_ADD8        I2C->I2C_OAR1.ADD8      //Interface address???
//#define I2C_ABK         I2C->I2C_OAR1.ABK       //Should always be kept at 1 by software
#define I2C_ADDMODE     I2C->I2C_OAR1.ADDMODE   //Addressing mode (slave mode)
//I2C Own address register 2
#define I2C_ENDUAL      I2C->I2C_OAR2.ENDUAL    //Dual addressing mode enable
#define I2C_ADD2        I2C->I2C_OAR2.ADD2      //Interface address  
//I2C Data register
#define I2C_DR          I2C->I2C_DR.DR          //8-bit data register  
//I2C Status register 1
#define I2C_SB          I2C->I2C_SR1.SB         //Start bit (Master mode)
#define I2C_ADDR        I2C->I2C_SR1.ADDR       //Address sent (master mode)/matched (slave mode)
#define I2C_BTF         I2C->I2C_SR1.BTF        //Byte transfer finished
#define I2C_ADD10       I2C->I2C_SR1.ADD10      //10-bit header sent (Master mode)
#define I2C_STOPF       I2C->I2C_SR1.STOPF      //Stop detection (slave mode)  
#define I2C_RxNE        I2C->I2C_SR1.RxNE       //Data register not empty (receivers)
#define I2C_TxE         I2C->I2C_SR1.TxE        //Data register empty (transmitters)
#define I2C_BERR        I2C->I2C_SR1.BERR       //Bus error
#define I2C_ARLO        I2C->I2C_SR1.ARLO       //Arbitration lost (master mode)
#define I2C_AF          I2C->I2C_SR1.AF         //Acknowledge failure
#define I2C_OVR         I2C->I2C_SR1.OVR        //Overrun/Underrun
#define I2C_PECERR      I2C->I2C_SR1.PECERR     //PEC Error in reception 
#define I2C_TIMEOUT     I2C->I2C_SR1.TIMEOUT    //Timeout or Tlow error
#define I2C_SMBALERT    I2C->I2C_SR1.SMBALERT   //SMBus alert 
//I2C Status register 2
#define I2C_MSL         I2C->I2C_SR2.MSL        //Master/slave
#define I2C_BUSY        I2C->I2C_SR2.BUSY       //Bus busy
#define I2C_TRA         I2C->I2C_SR2.TRA        //Transmitter/receiver
#define I2C_GENCALL     I2C->I2C_SR2.GENCALL    //General call address (Slave mode)
#define I2C_SMBDEFAULT  I2C->I2C_SR2.SMBDEFAULT //SMBus device default address (Slave mode)
#define I2C_SMBHOST     I2C->I2C_SR2.SMBHOST    //SMBus host header (Slave mode)
#define I2C_DUALF       I2C->I2C_SR2.DUALF      //Dual flag (Slave mode)
#define I2C_PEC         I2C->I2C_SR2.PEC        //Packet error checking register
//I2C Clock control register 
#define I2C_CCR         I2C->I2C_CCR.CCR               //Clock control register in Fm/Sm mode (Master mode)
#define I2C_DUTY        I2C->I2C_CCR.DUTY              //Fm mode duty cycle
#define I2C_FS          I2C->I2C_CCR.FS                 //I2C master mode selection
//I2C TRISE register
#define I2C_TRISE       I2C->I2C_TRISE.TRISE             //Maximum rise time in Fm/Sm mode (Master mode)

/******************************************* USART**************************************************/
typedef struct
{
  struct
  {
    unsigned PE         : 1; //Parity error
    unsigned FE         : 1; //Framing error
    unsigned NE         : 1; //Noise error flag
    unsigned ORE        : 1; //Overrun error
    unsigned IDLE       : 1; //IDLE line detected
    unsigned RXNE       : 1; //Read data register not empty
    unsigned TC         : 1; //Transmission complete
    unsigned TXE        : 1; //Transmit data register empty
    unsigned LBD        : 1; //LIN break detection flag
    unsigned CTS        : 1; //CTS flag
    unsigned RSVD0      : 22; //Reserved      
  }volatile USART_SR;         //Status register
  struct
  {
    unsigned DR1         : 9; //Reserved
    unsigned RSVD0      : 23;//Reserved      
  }volatile USART_DR1;       //Data register
  struct
  {
    //unsigned DIV_Fraction       : 4;  //fraction of USARTDIV
    //unsigned DIV_Mantissa       : 12; //mantissa of USARTDIV
    unsigned BRR                : 16; //(APB1+BRR/2)/BRR
    unsigned RSVD0              : 16; //Reserved 
    
  }volatile USART_BRR;                //Baud rate register
  struct
  {
    unsigned SBK        : 1; //Send break
    unsigned RWU        : 1; //Receiver wakeup
    unsigned RE         : 1; //Receiver enable
    unsigned TE         : 1; //Transmitter enable
    unsigned IDLEIE     : 1; //IDLE interrupt enable
    unsigned RXNEIE     : 1; //RXNE interrupt enable
    unsigned TCIE       : 1; //Transmission complete interrupt enable
    unsigned TXEIE      : 1; //TXE interrupt enable
    unsigned PEIE       : 1; //PE interrupt enable
    unsigned PS         : 1; //Parity selection
    unsigned PCE        : 1; //Parity control enable
    unsigned WAKE       : 1; //Wakeup method
    unsigned M          : 1; //Word length
    unsigned UE         : 1; //USART enable
    unsigned RSVD0      : 18; //Reserved      
  }volatile USART_CR1;         //Status register
  struct
  {
    unsigned ADD        : 4; //Address of the USART node
    unsigned RSVD2      : 1; //Reserved
    unsigned LBDL       : 1; //lin break detection length
    unsigned LBDIE      : 1; //LIN break detection interrupt enable
    unsigned RSVD1      : 1; //Reserved
    unsigned LBCL       : 1; //Last bit clock pulse
    unsigned CPHA       : 1; //Clock phase
    unsigned CPOL       : 1; //Clock polarity
    unsigned CLKEN      : 1; //Clock enable
    unsigned STOP       : 2; //STOP bits
    unsigned LINEN      : 1; //LIN mode enable
    unsigned RSVD0      : 17; //Reserved      
  }volatile USART_CR2;         //Status register
  struct
  {
    unsigned EIE        : 1; //Error interrupt enable
    unsigned IREN       : 1; //IrDA mode enable
    unsigned IRLP       : 1; //IrDA low-power
    unsigned HDSEL      : 1; //Half-duplex selection
    unsigned NACK       : 1; //Smartcard NACK enable
    unsigned SCEN       : 1; //Smartcard mode enable
    unsigned DMAR       : 1; //DMA enable receiver
    unsigned DMAT       : 1; //DMA enable transmitter
    unsigned RTSE       : 1; //RTS enable
    unsigned CTSE       : 1; //CTS enable
    unsigned CTSIE      : 1; //CTS interrupt enable
    unsigned RSVD0      : 21; //Reserved      
  }volatile USART_CR3;         //Status register
  struct
  {
    unsigned PSC        : 8; //Prescaler value
    unsigned GT         : 8; //Guard time value
    unsigned RSVD0      : 16; //Reserved      
  }volatile USART_GTPR;         //Guard time and prescaler register
}USART_Type;

#define USART ((USART_Type*) UART1_BASE)

//Status register (USART_SR)
#define USART_PE        USART->USART_SR.PE      //Parity error
#define USART_FE        USART->USART_SR.FE      //Framing error
#define USART_NE        USART->USART_SR.NE      //Noise error flag
#define USART_ORE       USART->USART_SR.ORE     //Overrun error
#define USART_IDLE      USART->USART_SR.IDLE    //IDLE line detected
#define USART_RXNE      USART->USART_SR.RXNE    //Read data register not empty
#define USART_TC        USART->USART_SR.TC      //Transmission complete
#define USART_TXE       USART->USART_SR.TXE     //Transmit data register empty
#define USART_LBD       USART->USART_SR.LBD     //LIN break detection flag
#define USART_CTS       USART->USART_SR.CTS     //CTS flag     

//Data register (USART_DR)
#define USART_DR        USART->USART_DR1.DR1      //Reserved 

//Baud rate register (USART_BRR)
#define USART_BRR      USART->USART_BRR.BRR //fraction of USARTDIV
#define USART_DIV_Fraction      USART->USART_BRR.DIV_Fraction //fraction of USARTDIV
#define USART_DIV_Mantissa      USART->USART_BRR.DIV_Mantissa //mantissa of USARTDIV

//Control register 1 (USART_CR1)
#define USART_SBK       USART->USART_CR1.SBK    //Send break
#define USART_RWU       USART->USART_CR1.RWU    //Receiver wakeup
#define USART_RE        USART->USART_CR1.RE     //Receiver enable
#define USART_TE        USART->USART_CR1.TE     //Transmitter enable
#define USART_IDLEIE    USART->USART_CR1.IDLEIE //IDLE interrupt enable
#define USART_RXNEIE    USART->USART_CR1.RXNEIE //RXNE interrupt enable
#define USART_TCIE      USART->USART_CR1.TCIE   //Transmission complete interrupt enable
#define USART_TXEIE     USART->USART_CR1.TXEIE  //TXE interrupt enable
#define USART_PEIE      USART->USART_CR1.PEIE   //PE interrupt enable
#define USART_PS        USART->USART_CR1.PS     //Parity selection
#define USART_PCE       USART->USART_CR1.PCE    //Parity control enable
#define USART_WAKE      USART->USART_CR1.WAKE   //Wakeup method
#define USART_M         USART->USART_CR1.M      //Word length
#define USART_UE        USART->USART_CR1.UE     //USART enable      
  
//Control register 2 (USART_CR2)
#define USART_ADD       USART->USART_CR2.ADD    //Address of the USART node
#define USART_LBDL      USART->USART_CR2.LBDL   //lin break detection length
#define USART_LBDIE     USART->USART_CR2.LBDIE  //LIN break detection interrupt enable
#define USART_LBCL      USART->USART_CR2.LBCL   //Last bit clock pulse
#define USART_CPHA      USART->USART_CR2.CPHA   //Clock phase
#define USART_CPOL      USART->USART_CR2.CPOL   //Clock polarity
#define USART_CLKEN     USART->USART_CR2.CLKEN  //Clock enable
#define USART_STOP      USART->USART_CR2.STOP   //STOP bits
#define USART_LINEN     USART->USART_CR2.LINEN  //LIN mode enable     
  
//Control register 3 (USART_CR3)
#define USART_EIE       USART->USART_CR3.EIE    //Error interrupt enable
#define USART_IREN      USART->USART_CR3.IREN   //IrDA mode enable
#define USART_IRLP      USART->USART_CR3.IRLP   //IrDA low-power
#define USART_HDSEL     USART->USART_CR3.HDSEL  //Half-duplex selection
#define USART_NACK      USART->USART_CR3.NACK   //Smartcard NACK enable
#define USART_SCEN      USART->USART_CR3.SCEN   //Smartcard mode enable
#define USART_DMAR      USART->USART_CR3.DMAR   //DMA enable receiver
#define USART_DMAT      USART->USART_CR3.DMAT   //DMA enable transmitter
#define USART_RTSE      USART->USART_CR3.RTSE   //RTS enable
#define USART_CTSE      USART->USART_CR3.CTSE   //CTS enable
#define USART_CTSIE     USART->USART_CR3.CTSIE  //CTS interrupt enable    

//Guard time and prescaler register (USART_GTPR)
#define USART_PSC       USART->USART_GTPR.PSC   //Prescaler value
#define USART_GT        USART->USART_GTPR.GT    //Guard time value     
/******************************************* END **************************************************/
#ifdef __cplusplus
}
#endif
