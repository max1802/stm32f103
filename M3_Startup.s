        MODULE  ?cstartup
        SECTION CSTACK:DATA:NOROOT(3)
        SECTION .intvec:CODE:NOROOT(2)
        EXTERN  __iar_program_start      
        PUBLIC  __vector_table
        DATA
__vector_table
        DCD     sfe(CSTACK)                     ; 0x0000 STACK
        DCD     Reset_Handler                   ; 0x0004 Reset Handler
        DCD     NMI_Handler                     ; 0x0008 NMI Handler
        DCD     HardFault_Handler               ; 0x000C Hard Fault Handler
        DCD     MemManage_Handler               ; 0x0010 MPU Fault Handler
        DCD     BusFault_Handler                ; 0x0014 Bus Fault Handler
        DCD     UsageFault_Handler              ; 0x0018 Usage Fault Handler
        DCD     0                               ; 0x001C Reserved
        DCD     0                               ; 0x0020 Reserved
        DCD     0                               ; 0x0024 Reserved
        DCD     0                               ; 0x0028 Reserved
        DCD     SVC_Handler                     ; 0x002C SVCall Handler
        DCD     DebugMon_Handler                ; 0x0030 0x00 Debug Monitor Handler
        DCD     0                               ; 0x0034 Reserved
        DCD     PendSV_Handler                  ; 0x0038 PendSV Handler
        DCD     SysTick_Handler                 ; 0x003C SysTick Handler
        DCD     WWDG_Handler                    ; 0x0040 Window Watchdog
        DCD     PVD_Handler                     ; 0x0044 PVD through EXTI Line detect
        DCD     TAMPER_Handler                  ; 0x0048 Tamper
        DCD     RTC_Handler                     ; 0x004C RTC
        DCD     FLASH_Handler                   ; 0x0050 Flash
        DCD     RCC_Handler                     ; 0x0054 RCC
        DCD     EXTI0_Handler                   ; 0x0058 EXTI Line 0
        DCD     EXTI1_Handler                   ; 0x005C EXTI Line 1
        DCD     EXTI2_Handler                   ; 0x0060 EXTI Line 2
        DCD     EXTI3_Handler                   ; 0x0064 EXTI Line 3
        DCD     EXTI4_Handler                   ; 0x0068 EXTI Line 4
        DCD     DMA1_CH1_Handler                ; 0x006C DMA1 Channel 1
        DCD     DMA1_CH2_Handler                ; 0x0070 DMA1 Channel 2
        DCD     DMA1_CH3_Handler                ; 0x0074 DMA1 Channel 3
        DCD     DMA1_CH4_Handler                ; 0x0078 DMA1 Channel 4
        DCD     DMA1_CH5_Handler                ; 0x007C DMA1 Channel 5
        DCD     DMA1_CH6_Handler                ; 0x0080 DMA1 Channel 6
        DCD     DMA1_CH7_Handler                ; 0x0084 DMA1 Channel 7
        DCD     ADC1_2_Handler                  ; 0x0088 ADC1
        DCD     USB_HP_CAN_TX_Handler           ; 0x008C USB High Priority
        DCD     USB_LP_CAN_RX0_Handler          ; 0x0090 USB Low  Priority
        DCD     CAN_RX1_Handler                 ; 0x0094 CAN1 RX1
        DCD     CAN_SCE_Handler                 ; 0x0098 CAN1 SCE
        DCD     EXTI9_5_Handler                 ; 0x009C EXTI Line 9..5
        DCD     TIM1_9_Handler                  ; 0x00A0 TIM1 Break and TIM9
        DCD     TIM1_10_Handler                 ; 0x00A4 TIM1 Update and TIM10
        DCD     TIM1_11_Handler                 ; 0x00A8 TIM1 Trigger and Commutation and TIM11
        DCD     TIM1_CC_Handler                 ; 0x00AC TIM1 Capture Compare
        DCD     TIM2_Handler                    ; 0x00B0 TIM2
        DCD     TIM3_Handler                    ; 0x00B4 TIM3
        DCD     TIM4_Handler                    ; 0x00B8 TIM4
        DCD     I2C1_EV_Handler                 ; 0x00BC I2C1 Event
        DCD     I2C1_ER_Handler                 ; 0x00C0 I2C1 Error
        DCD     I2C2_EV_Handler                 ; 0x00C4 I2C2 Event
        DCD     I2C2_ER_Handler                 ; 0x00C8 I2C2 Error
        DCD     SPI1_Handler                    ; 0x00CC SPI1
        DCD     SPI2_Handler                    ; 0x00D0 SPI2
        DCD     UART1_Handler                   ; 0x00D4 USART1
        DCD     UART2_Handler                   ; 0x00D8 USART2
        DCD     UART3_Handler                   ; 0x00DC USART3
        DCD     EXTI15_10_Handler               ; 0x00E0 EXTI Line 15..10
        DCD     RTC_Alarm_Handler               ; 0x00E4 RTC Alarm through EXTI Line
        DCD     USBWakeUp_Handler               ; 0x00E8 USB Wakeup from suspend
        DCD     TIM8_12_Handler                 ; 0x00EC TIM8 Break and TIM12
        DCD     TIM8_13_Handler                 ; 0x00F0 TIM8 Update and TIM13
        DCD     TIM8_14_Handler                 ; 0x00F4 TIM8 Trigger and Commutation and TIM14
        DCD     TIM8_CC_Handler                 ; 0x00F8 TIM8 Capture Compare
        DCD     ADC3_Handler                    ; 0x00FC ADC3
        DCD     FSMC_Handler                    ; 0x0100 FSMC
        DCD     SDIO_Handler                    ; 0x0104 SDIO
        DCD     TIM5_Handler                    ; 0x0108 TIM5
        DCD     SPI3_Handler                    ; 0x010C SPI3
        DCD     UART4_Handler                   ; 0x0110 UART4
        DCD     UART5_Handler                   ; 0x0114 UART5
        DCD     TIM6_Handler                    ; 0x0118 TIM6 and DAC1&2 underrun errors
        DCD     TIM7_Handler                    ; 0x011C TIM7
        DCD     DMA2_CH1_Handler                ; 0x0120 DMA2 Stream 1
        DCD     DMA2_CH2_Handler                ; 0x0124 DMA2 Stream 2
        DCD     DMA2_CH3_Handler                ; 0x0128 DMA2 Stream 3
        DCD     DMA2_CH4_5_Handler              ; 0x012C DMA2 Stream 4 and 5
        DCD     DMA2_CH5_Handler                ; 0x0130 DMA2 Stream 5
        DCD     ETH_Handler                     ; 0x0134 Ethernet
        DCD     ETH_WKUP_Handler                ; 0x0138 Ethernet Wakeup through EXTI line
        DCD     CAN2_TX_Handler                 ; 0x013C CAN2 TX
        DCD     CAN2_RX0_Handler                ; 0x0140 CAN2 RX0
        DCD     CAN2_RX1_Handler                ; 0x0144 CAN2 RX1
        DCD     CAN2_SCE_Handler                ; 0x0148 CAN2 SCE
        DCD     OTG_FS_Handler                  ; 0x014C USB OTG FS
        
        THUMB
        
        PUBWEAK Reset_Handler
        SECTION .text:CODE:REORDER:NOROOT(2)
Reset_Handler
        LDR     R0, =__iar_program_start
        BX      R0
        
        PUBWEAK NMI_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
NMI_Handler
        B NMI_Handler

        PUBWEAK HardFault_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
HardFault_Handler
        B HardFault_Handler

        PUBWEAK MemManage_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
MemManage_Handler
        B MemManage_Handler

        PUBWEAK BusFault_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
BusFault_Handler
        B BusFault_Handler

        PUBWEAK UsageFault_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
UsageFault_Handler
        B UsageFault_Handler

        PUBWEAK SVC_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
SVC_Handler
        B SVC_Handler

        PUBWEAK DebugMon_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
DebugMon_Handler
        B DebugMon_Handler

        PUBWEAK PendSV_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
PendSV_Handler
        B PendSV_Handler

        PUBWEAK SysTick_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
SysTick_Handler
        B SysTick_Handler

        PUBWEAK WWDG_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
WWDG_Handler
        B WWDG_Handler

        PUBWEAK PVD_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
PVD_Handler
        B PVD_Handler

        PUBWEAK TAMPER_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
TAMPER_Handler
        B TAMPER_Handler

        PUBWEAK RTC_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
RTC_Handler
        B RTC_Handler

        PUBWEAK FLASH_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
FLASH_Handler
        B FLASH_Handler

        PUBWEAK RCC_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
RCC_Handler
        B RCC_Handler

        PUBWEAK EXTI0_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
EXTI0_Handler
        B EXTI0_Handler

        PUBWEAK EXTI1_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
EXTI1_Handler
        B EXTI1_Handler

        PUBWEAK EXTI2_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
EXTI2_Handler
        B EXTI2_Handler

        PUBWEAK EXTI3_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
EXTI3_Handler
        B EXTI3_Handler

        PUBWEAK EXTI4_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
EXTI4_Handler
        B EXTI4_Handler

        PUBWEAK DMA1_CH1_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
DMA1_CH1_Handler
        B DMA1_CH1_Handler

        PUBWEAK DMA1_CH2_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
DMA1_CH2_Handler
        B DMA1_CH2_Handler

        PUBWEAK DMA1_CH3_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
DMA1_CH3_Handler
        B DMA1_CH3_Handler

        PUBWEAK DMA1_CH4_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
DMA1_CH4_Handler
        B DMA1_CH4_Handler

        PUBWEAK DMA1_CH5_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
DMA1_CH5_Handler
        B DMA1_CH5_Handler

        PUBWEAK DMA1_CH6_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
DMA1_CH6_Handler
        B DMA1_CH6_Handler

        PUBWEAK DMA1_CH7_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
DMA1_CH7_Handler
        B DMA1_CH7_Handler

        PUBWEAK ADC1_2_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
ADC1_2_Handler
        B ADC1_2_Handler
 
        PUBWEAK USB_HP_CAN_TX_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
USB_HP_CAN_TX_Handler
        B USB_HP_CAN_TX_Handler
        
        PUBWEAK USB_LP_CAN_RX0_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
USB_LP_CAN_RX0_Handler
        B USB_LP_CAN_RX0_Handler
        
        PUBWEAK CAN_RX1_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
CAN_RX1_Handler
        B CAN_RX1_Handler
        
        PUBWEAK CAN_SCE_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
CAN_SCE_Handler
        B CAN_SCE_Handler
        
        PUBWEAK EXTI9_5_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
EXTI9_5_Handler
        B EXTI9_5_Handler
        
        PUBWEAK TIM1_9_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM1_9_Handler
        B TIM1_9_Handler
        
        PUBWEAK TIM1_10_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM1_10_Handler
        B TIM1_10_Handler
        
        PUBWEAK TIM1_11_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM1_11_Handler
        B TIM1_11_Handler
        
        PUBWEAK TIM1_CC_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM1_CC_Handler
        B TIM1_CC_Handler

        PUBWEAK TIM2_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM2_Handler
        B TIM2_Handler

        PUBWEAK TIM3_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM3_Handler
        B TIM3_Handler

        PUBWEAK TIM4_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM4_Handler
        B TIM4_Handler

        PUBWEAK I2C1_EV_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
I2C1_EV_Handler
        B I2C1_EV_Handler

        PUBWEAK I2C1_ER_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
I2C1_ER_Handler
        B I2C1_ER_Handler

        PUBWEAK I2C2_EV_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
I2C2_EV_Handler
        B I2C2_EV_Handler

        PUBWEAK I2C2_ER_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
I2C2_ER_Handler
        B I2C2_ER_Handler

        PUBWEAK SPI1_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
SPI1_Handler
        B SPI1_Handler

        PUBWEAK SPI2_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
SPI2_Handler
        B SPI2_Handler

        PUBWEAK UART1_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
UART1_Handler
        B UART1_Handler

        PUBWEAK UART2_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
UART2_Handler
        B UART2_Handler

        PUBWEAK UART3_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
UART3_Handler
        B UART3_Handler

        PUBWEAK EXTI15_10_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
EXTI15_10_Handler
        B EXTI15_10_Handler

        PUBWEAK RTC_Alarm_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
RTC_Alarm_Handler
        B RTC_Alarm_Handler

        PUBWEAK USBWakeUp_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
USBWakeUp_Handler
        B USBWakeUp_Handler
        
        PUBWEAK TIM8_12_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM8_12_Handler
        B TIM8_12_Handler
        
        PUBWEAK TIM8_13_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM8_13_Handler
        B TIM8_13_Handler
        
        PUBWEAK TIM8_14_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM8_14_Handler
        B TIM8_14_Handler
        
        PUBWEAK TIM8_CC_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM8_CC_Handler
        B TIM8_CC_Handler
        
        PUBWEAK ADC3_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
ADC3_Handler
        B ADC3_Handler
        
        PUBWEAK FSMC_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
FSMC_Handler
        B FSMC_Handler
        
        PUBWEAK SDIO_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
SDIO_Handler
        B SDIO_Handler
        
        PUBWEAK TIM5_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM5_Handler
        B TIM5_Handler
        
        PUBWEAK SPI3_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
SPI3_Handler
        B SPI3_Handler
        
        PUBWEAK UART4_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
UART4_Handler
        B UART4_Handler
        
        PUBWEAK UART5_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
UART5_Handler
        B UART5_Handler
        
        PUBWEAK TIM6_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM6_Handler
        B TIM6_Handler
        
        PUBWEAK TIM7_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
TIM7_Handler
        B TIM7_Handler
        
        PUBWEAK DMA2_CH1_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
DMA2_CH1_Handler
        B DMA2_CH1_Handler
        
        PUBWEAK DMA2_CH2_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
DMA2_CH2_Handler
        B DMA2_CH2_Handler
        
        PUBWEAK DMA2_CH3_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
DMA2_CH3_Handler
        B DMA2_CH3_Handler
        
        PUBWEAK DMA2_CH4_5_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
DMA2_CH4_5_Handler
        B DMA2_CH4_5_Handler
        
        PUBWEAK DMA2_CH5_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
DMA2_CH5_Handler
        B DMA2_CH5_Handler
        
        PUBWEAK ETH_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
ETH_Handler
        B ETH_Handler
        
        PUBWEAK ETH_WKUP_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
ETH_WKUP_Handler
        B ETH_WKUP_Handler
        
        PUBWEAK CAN2_TX_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
CAN2_TX_Handler
        B CAN2_TX_Handler
        
        PUBWEAK CAN2_RX0_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
CAN2_RX0_Handler
        B CAN2_RX0_Handler
        
        PUBWEAK CAN2_RX1_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
CAN2_RX1_Handler
        B CAN2_RX1_Handler
        
        PUBWEAK CAN2_SCE_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
CAN2_SCE_Handler
        B CAN2_SCE_Handler
        
        PUBWEAK OTG_FS_Handler
        SECTION .text:CODE:REORDER:NOROOT(1)
OTG_FS_Handler
        B OTG_FS_Handler
        
        END
